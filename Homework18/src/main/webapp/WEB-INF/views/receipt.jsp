<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<body>
<br><br>
<h1 align=center> Dear <c:out value="${sessionScope.name}"/>, your order</h1>
<br><br>
<c:forEach var="product" items="${currentUserBasket}" varStatus = "loop">
       <div align=center> ${loop.index+1}) ${product} </div>
<br>
</c:forEach>
<br>
<div align=center>Total: $ ${totalAmount} </div>
</body>
</html>