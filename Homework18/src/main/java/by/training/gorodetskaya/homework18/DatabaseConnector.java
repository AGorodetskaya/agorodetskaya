package by.training.gorodetskaya.homework18;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnector {

    private static final String DB_PROPS_PATH = "/db.properties";
    private static final String DB_DRIVER = "db.driver";
    private static final String DB_URL = "db.url";
    private static final String DB_USERNAME = "db.username";
    private static final String DB_PASSWORD = "db.password";
    private static final String DB_KEEPER_OPENED = "db.keeper.opened";
    private static final String DB_INIT = "db.init";

    private String driverName;
    private String databaseURL;
    private String username;
    private String password;
    private String initDbSql;
    private String keeper;

    private static DatabaseConnector connector;

    private DatabaseConnector() {
    }

    public static synchronized DatabaseConnector getConnector() {
        if (connector == null) {
            connector = new DatabaseConnector();
        }
        return connector;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDatabaseURL() {
        return databaseURL;
    }

    public void setDatabaseURL(String databaseURL) {
        this.databaseURL = databaseURL;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getInitDbSql() {
        return initDbSql;
    }

    public void setInitDbSql(String initDbSql) {
        this.initDbSql = initDbSql;
    }

    public String getKeeper() {
        return keeper;
    }

    public void setKeeper(String keeper) {
        this.keeper = keeper;
    }

    public void initProperties() {
        Properties props = new Properties();
        try (InputStream inp = this.getClass().getClassLoader().getResourceAsStream(DB_PROPS_PATH)) {
            props.load(inp);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        driverName = props.getProperty(DB_DRIVER);
        databaseURL = props.getProperty(DB_URL);
        username = props.getProperty(DB_USERNAME);
        password = props.getProperty(DB_PASSWORD);
        keeper = props.getProperty(DB_KEEPER_OPENED);
        initDbSql = props.getProperty(DB_INIT);
    }

    public void initDatabase() {
        try (Connection connection = DriverManager.getConnection(databaseURL + keeper + initDbSql, username, password)) {
            Class.forName(driverName);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(databaseURL + keeper, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}