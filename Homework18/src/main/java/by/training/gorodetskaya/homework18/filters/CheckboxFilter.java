package by.training.gorodetskaya.homework18.filters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

@WebFilter(urlPatterns = {"/order", "/confirm"})
public class CheckboxFilter implements Filter {

    private static final String AGREEMENT = "agreement";
    private static final String ERROR_PAGE = "/error";
    private static final Logger LOG = LogManager.getLogger(CheckboxFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        if (Objects.isNull(session.getAttribute(AGREEMENT))) {
            response.sendRedirect(request.getContextPath() + ERROR_PAGE);
            LOG.debug("The agreement checkbox has not been checked");
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}