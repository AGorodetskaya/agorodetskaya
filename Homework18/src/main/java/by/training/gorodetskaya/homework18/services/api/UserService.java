package by.training.gorodetskaya.homework18.services.api;

import by.training.gorodetskaya.homework18.entity.User;

public interface UserService {

    User addUser(String name);

    User getOrCreateUserByName(String name);
}
