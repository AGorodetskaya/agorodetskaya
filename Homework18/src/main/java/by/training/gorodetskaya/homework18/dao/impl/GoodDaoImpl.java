package by.training.gorodetskaya.homework18.dao.impl;

import by.training.gorodetskaya.homework18.DatabaseConnector;
import by.training.gorodetskaya.homework18.dao.api.GoodDao;
import by.training.gorodetskaya.homework18.entity.Good;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class GoodDaoImpl implements GoodDao {

    private static final String SELECT_ALL_GOODS = "SELECT ID, TITLE, PRICE FROM GOOD";
    private static final String SELECT_GOOD_BY_ID = "SELECT ID, TITLE, PRICE FROM GOOD WHERE ID=?";
    private static final String ID = "ID";
    private static final String TITLE = "TITLE";
    private static final String PRICE = "PRICE";

    private final DatabaseConnector connector;

    public GoodDaoImpl(DatabaseConnector connector) {
        this.connector = connector;
    }

    @Override
    public Set<Good> getAllGoods() {
        Set<Good> priceList = new HashSet<>();
        try (Connection connection = connector.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_GOODS);

            while (resultSet.next()) {
                Good good = new Good();
                good.setId(resultSet.getInt(ID));
                good.setTitle(resultSet.getString(TITLE));
                good.setPrice(resultSet.getBigDecimal(PRICE));
                priceList.add(good);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return priceList;
    }

    @Override
    public Good getGoodById(int id) {
        Good good = new Good();
        try (Connection connection = connector.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_GOOD_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                good.setId(resultSet.getInt(ID));
                good.setTitle(resultSet.getString(TITLE));
                good.setPrice(resultSet.getBigDecimal(PRICE));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return good;
    }
}