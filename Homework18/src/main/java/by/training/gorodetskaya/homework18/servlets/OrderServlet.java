package by.training.gorodetskaya.homework18.servlets;

import by.training.gorodetskaya.homework18.entity.Good;
import by.training.gorodetskaya.homework18.entity.Order;
import by.training.gorodetskaya.homework18.services.api.GoodService;
import by.training.gorodetskaya.homework18.services.api.OrderService;
import by.training.gorodetskaya.homework18.services.api.UserService;
import by.training.gorodetskaya.homework18.services.impl.GoodServiceImpl;
import by.training.gorodetskaya.homework18.services.impl.OrderServiceImpl;
import by.training.gorodetskaya.homework18.services.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(asyncSupported = false, name = "Order page", urlPatterns = "/order")
public class OrderServlet extends HttpServlet {

    private static final String NAME = "name";
    private static final String ID = "id";


    private static final String ORDER_PAGE = "/order";
    private static final String ORDER_JSP = "WEB-INF/views/order.jsp";
    private static final String RANGE = "range";
    private static final String USER_BASKET = "currentUserBasket";

    private UserService userService;
    private GoodService goodService;
    private OrderService orderService;

    @Override
    public void init() throws ServletException {
        userService = UserServiceImpl.getInstance();
        goodService = GoodServiceImpl.getInstance();
        orderService = OrderServiceImpl.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        req.setAttribute(RANGE, goodService.getAllGoods());
        req.setAttribute(USER_BASKET, orderService.getUserGoods(getUserName(session)));

        req.getRequestDispatcher(ORDER_JSP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String id = request.getParameter(ID);
        String name = getUserName(session);

        Good good = goodService.getGoodById(Integer.parseInt(id));
        Order order = orderService.getOrderByUserId(userService.getOrCreateUserByName(name).getId());
        orderService.addGoodInOrder(order, good);
        orderService.updateAmountOfOrder(name);

        resp.sendRedirect(request.getContextPath() + ORDER_PAGE);
    }

    private String getUserName(HttpSession session) {
        return session.getAttribute(NAME).toString();
    }

}
