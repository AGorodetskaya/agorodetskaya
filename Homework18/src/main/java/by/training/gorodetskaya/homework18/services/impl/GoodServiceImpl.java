package by.training.gorodetskaya.homework18.services.impl;

import by.training.gorodetskaya.homework18.DatabaseConnector;
import by.training.gorodetskaya.homework18.dao.api.GoodDao;
import by.training.gorodetskaya.homework18.dao.impl.GoodDaoImpl;
import by.training.gorodetskaya.homework18.entity.Good;
import by.training.gorodetskaya.homework18.services.api.GoodService;

import java.util.Set;

public class GoodServiceImpl implements GoodService {

    private static GoodServiceImpl instance;

    private final GoodDao goodDao = new GoodDaoImpl(DatabaseConnector.getConnector());

    private GoodServiceImpl() {
    }

    public static synchronized GoodServiceImpl getInstance() {
        if (instance == null) {
            instance = new GoodServiceImpl();
        }
        return instance;
    }

    public Good getGoodById(int id) {
        return goodDao.getGoodById(id);
    }

    public Set<Good> getAllGoods() {
        return goodDao.getAllGoods();
    }
}