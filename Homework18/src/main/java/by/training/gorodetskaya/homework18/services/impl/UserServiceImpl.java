package by.training.gorodetskaya.homework18.services.impl;

import by.training.gorodetskaya.homework18.DatabaseConnector;
import by.training.gorodetskaya.homework18.dao.api.UserDao;
import by.training.gorodetskaya.homework18.dao.impl.UserDaoImpl;
import by.training.gorodetskaya.homework18.entity.User;
import by.training.gorodetskaya.homework18.services.api.OrderService;
import by.training.gorodetskaya.homework18.services.api.UserService;

import java.util.Optional;

public class UserServiceImpl implements UserService {

    private final UserDao userDao = new UserDaoImpl(DatabaseConnector.getConnector());

    private static UserServiceImpl instance;

    private static OrderService orderService = OrderServiceImpl.getInstance();

    public UserServiceImpl() {
    }

    public static synchronized UserServiceImpl getInstance() {
        if (instance == null) {
            instance = new UserServiceImpl();
        }
        return instance;
    }

    public User addUser(String name) {
        User user = new User();
        user.setLogin(name);
        return userDao.addUser(user);
    }

    public User getOrCreateUserByName(String name) {
        Optional<User> userOptional = userDao.getUserByName(name);
        if (userOptional.isPresent()) {
            return userOptional.get();
        } else {
            User user = addUser(name);
            orderService.addOrder(user.getId());
            return getOrCreateUserByName(name);
        }
    }
}