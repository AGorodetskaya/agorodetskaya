package by.training.gorodetskaya.homework18.servlets;

import by.training.gorodetskaya.homework18.services.api.OrderService;
import by.training.gorodetskaya.homework18.services.impl.OrderServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(asyncSupported = false, name = "Confirm page", urlPatterns = "/confirm")
public class ReceiptServlet extends HttpServlet {

    private static final String NAME = "name";
    private static final String USER_BASKET = "currentUserBasket";
    private static final String TOTAL_AMOUNT = "totalAmount";
    private static final String RECEIPT_JSP = "WEB-INF/views/receipt.jsp";
    private static final String ID = "id";

    private OrderService orderService;

    @Override
    public void init() throws ServletException {
        orderService = OrderServiceImpl.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String name = session.getAttribute(NAME).toString();
        Integer id = (Integer) session.getAttribute(ID);
        req.setAttribute(USER_BASKET, orderService.getUserGoods(name));
        req.setAttribute(TOTAL_AMOUNT, orderService.getOrderByUserId(id).getTotalPrice());

        req.getRequestDispatcher(RECEIPT_JSP).forward(req, resp);
    }
}