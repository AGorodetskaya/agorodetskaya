package by.training.gorodetskaya.homework18.servlets;

import by.training.gorodetskaya.homework18.entity.User;
import by.training.gorodetskaya.homework18.services.api.UserService;
import by.training.gorodetskaya.homework18.services.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(asyncSupported = false, name = "Welcome page", urlPatterns = "/welcome")
public class WelcomeServlet extends HttpServlet {

    private static final String NAME = "name";
    private static final String AGREEMENT = "agreement";
    private static final String ORDER_PAGE = "/order";
    private static final String WELCOME_JSP = "WEB-INF/views/welcome.jsp";
    private static final String ID = "id";

    private UserService userService;

    @Override
    public void init() throws ServletException {
        userService = UserServiceImpl.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(WELCOME_JSP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter(NAME);
        String agreement = req.getParameter(AGREEMENT);

        User user = userService.getOrCreateUserByName(name);

        HttpSession session = req.getSession();
        session.setAttribute(NAME, name);
        session.setAttribute(AGREEMENT, agreement);
        session.setAttribute(ID, user.getId());

        resp.sendRedirect(req.getContextPath() + ORDER_PAGE);
    }
}