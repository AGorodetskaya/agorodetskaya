package by.training.gorodetskaya.homework18;

import org.h2.tools.Server;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.SQLException;

@WebListener
public class DatabaseInitiator implements ServletContextListener {

    private static final String TCP = "-tcp";
    private static final String TCP_ALLOW_OTHERS = "-tcpAllowOthers";
    private static final String TCP_PORT = "-tcpPort";
    private static final String PORT = "9192";

    private final DatabaseConnector connector = DatabaseConnector.getConnector();
    private Server server;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        connector.initProperties();
        try {
            server = Server.createTcpServer(TCP, TCP_ALLOW_OTHERS, TCP_PORT, PORT).start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connector.initDatabase();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        server.stop();
    }
}