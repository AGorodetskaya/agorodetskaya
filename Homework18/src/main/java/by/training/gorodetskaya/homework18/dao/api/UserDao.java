package by.training.gorodetskaya.homework18.dao.api;

import by.training.gorodetskaya.homework18.entity.User;

import java.util.Optional;

public interface UserDao {

    User addUser(User user);

    Optional<User> getUserByName(String name);
}