package by.training.gorodetskaya.homework18.services.api;

import by.training.gorodetskaya.homework18.entity.Good;

import java.util.Set;

public interface GoodService {

    Good getGoodById(int id);

    Set<Good> getAllGoods();
}
