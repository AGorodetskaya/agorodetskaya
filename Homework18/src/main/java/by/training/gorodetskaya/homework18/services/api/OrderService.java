package by.training.gorodetskaya.homework18.services.api;

import by.training.gorodetskaya.homework18.entity.Good;
import by.training.gorodetskaya.homework18.entity.Order;

import java.util.List;

public interface OrderService {

    void addOrder(int userId);

    Order getOrderByUserId(int id);

    void updateAmountOfOrder(String userName);

    List<Good> getUserGoods(String userName);

    void addGoodInOrder(Order order, Good good);
}
