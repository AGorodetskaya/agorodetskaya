package by.training.gorodetskaya.homework18.services.impl;

import by.training.gorodetskaya.homework18.DatabaseConnector;
import by.training.gorodetskaya.homework18.dao.api.OrderDao;
import by.training.gorodetskaya.homework18.dao.impl.OrderDaoImpl;
import by.training.gorodetskaya.homework18.entity.Good;
import by.training.gorodetskaya.homework18.entity.Order;
import by.training.gorodetskaya.homework18.services.api.OrderService;

import java.util.List;

public class OrderServiceImpl implements OrderService {

    private static OrderServiceImpl instance;

    private final OrderDao orderDao = new OrderDaoImpl(DatabaseConnector.getConnector());

    private OrderServiceImpl() {
    }

    public static synchronized OrderServiceImpl getInstance() {
        if (instance == null) {
            instance = new OrderServiceImpl();
        }
        return instance;
    }

    public void addOrder(int userId) {
        orderDao.addOrder(userId);
    }

    public Order getOrderByUserId(int id) {
        return orderDao.getOrderByUserId(id);
    }

    public void updateAmountOfOrder(String userName) {
        orderDao.updateAmountOfOrder(userName);
    }

    public List<Good> getUserGoods(String userName) {
        return orderDao.getUserGoods(userName);
    }

    public void addGoodInOrder(Order order, Good good) {
        orderDao.addGoodInOrder(order, good);
    }
}

