package by.training.gorodetskaya.homework18.dao.api;

import by.training.gorodetskaya.homework18.entity.Good;

import java.util.Set;

public interface GoodDao {

    Set<Good> getAllGoods();

    Good getGoodById(int id);
}