package by.training.gorodetskaya.homework18.dao.api;

import by.training.gorodetskaya.homework18.entity.Good;
import by.training.gorodetskaya.homework18.entity.Order;

import java.util.List;

public interface OrderDao {

    void addOrder(int userId);

    Order getOrderByUserId(int id);

    void updateAmountOfOrder(String username);

    void addGoodInOrder(Order order, Good good);

    List<Good> getUserGoods(String username);
}