package by.training.gorodetskaya.homework9.task1;

import java.util.Comparator;

public class Sorter implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        return Integer.compare(calculateSumOfDigits(o1), calculateSumOfDigits(o2));
    }

    private int calculateSumOfDigits(Integer integer) {
        int sum = 0;
        while (integer >= 1) {
            sum = sum + integer % 10;
            integer = integer / 10;
        }
        return sum;
    }
}