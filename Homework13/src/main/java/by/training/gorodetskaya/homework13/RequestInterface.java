package by.training.gorodetskaya.homework13;

public interface RequestInterface {
    void sendGet(int id);
    void sendPost(String title);
}
