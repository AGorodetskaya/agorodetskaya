package by.training.gorodetskaya.homework13;

public class Strategy {

    private RequestInterface requestInterface;

    public RequestInterface getRequestInterface() {
        return requestInterface;
    }

    public void setRequestInterface(RequestInterface requestInterface) {
        this.requestInterface = requestInterface;
    }

    public void execute(String mode, String method, String argument) {
        if (mode.equals("1")) {
            requestInterface = new Connection();
        } else if (mode.equals("2")) {
            requestInterface = new Client();
        } else {
            System.out.println("Please, type 1 or 2");
        }
        chooseMethod(method, argument);
    }

    private void chooseMethod(String method, String argument) {
        if (method.equals("POST")) {
            requestInterface.sendPost(argument);
        } else if (method.equals("GET")) {
            requestInterface.sendGet(Integer.parseInt(argument));
        } else {
            System.out.println("Please, type GET or POST");
        }
    }
}