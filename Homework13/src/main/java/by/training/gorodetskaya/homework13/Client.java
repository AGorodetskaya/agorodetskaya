package by.training.gorodetskaya.homework13;

import com.google.gson.Gson;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class Client implements RequestInterface {
    private static final String JSONPLACEHOLDER_POSTS = "https://jsonplaceholder.typicode.com/posts/";
    private static final String ACCEPT = "application/json";
    private static final String PROPERTY = "application/json; charset=UTF-8";
    private static final String BODY = "message";

    @Override
    public void sendGet(int id) {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(JSONPLACEHOLDER_POSTS + id);
            ResponseHandler<String> responseHandler = httpResponse -> EntityUtils.toString(httpResponse.getEntity());
            String httpResponse = httpClient.execute(httpGet, responseHandler);
            Article article = new Gson().fromJson(httpResponse, Article.class);
            System.out.println(article);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendPost(String title) {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(JSONPLACEHOLDER_POSTS);
            String gson = new Gson().toJson(new Article(1, title, BODY));
            StringEntity entityObject = new StringEntity(gson);
            httpPost.setEntity(entityObject);
            httpPost.setHeader("Accept", ACCEPT);
            httpPost.setHeader("Content-type", PROPERTY);
            ResponseHandler<String> responseHandler = httpResponse -> EntityUtils.toString(httpResponse.getEntity());
            String httpResponse = httpClient.execute(httpPost, responseHandler);
            Article article = new Gson().fromJson(httpResponse, Article.class);
            System.out.println(article.toString());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}