package by.training.gorodetskaya.homework13;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Connection implements RequestInterface {

    private static final String JSONPLACEHOLDER_POSTS = "https://jsonplaceholder.typicode.com/posts/";
    private static final String PROPERTY = "application/json; charset=UTF-8";
    private static final String BODY = "message";

    private void readResponse(HttpURLConnection connection) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Article article = mapper.readValue(connection.getInputStream(), Article.class);
            System.out.println(article);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void sendGet(int id) {
        URL url;
        HttpURLConnection connection = null;
        try {
            url = new URL(JSONPLACEHOLDER_POSTS + id);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        readResponse(connection);
    }

    @Override
    public void sendPost(String title) {
        URL url;
        HttpURLConnection connection = null;
        try {
            url = new URL(JSONPLACEHOLDER_POSTS);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-type", PROPERTY);
            connection.setDoOutput(true);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        Article article = new Article(1, title, BODY);
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(connection.getOutputStream(), article);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        readResponse(connection);
    }
}