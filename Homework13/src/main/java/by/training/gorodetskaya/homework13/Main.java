package by.training.gorodetskaya.homework13;

public class Main {

    public static void main(String[] args) {

        String mode = args[0];
        String method = args[1];
        String argument = args[2];

        Strategy strategy = new Strategy();
        strategy.execute(mode, method, argument);
    }
}