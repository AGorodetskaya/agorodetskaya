package Homework2_1;

/**
 * Homework 2_1
 *
 * @author Anna Gorodetskaya
 * @version 1.0
 * @since 1.0
 */
public class Calculation {

    /**
     * Main method description - calculation according to given formula.
     *
     * @param args - program arguments from command line (args[0] for variable a; args[1] for variable p;
     *             args[2] for variable m1, args[3] for variable m2).
     */
    public static void main(String[] args) {
        String stringA = args[0];
        String stringP = args[1];
        String stringM1 = args[2];
        String stringM2 = args[3];
        int a = Integer.parseInt(stringA);
        int p = Integer.parseInt(stringP);
        double m1 = Double.parseDouble(stringM1);
        double m2 = Double.parseDouble(stringM2);
        CalculationG calculationG = new CalculationG();
        calculationG.calculateG(a, p, m1, m2);
    }
}