package Homework2_1;

/**
 * Homework 2_1
 *
 * @author Anna Gorodetskaya
 * @version 1.0
 * @since 1.0
 */
public class CalculationG {
    double G;

    /**
     * Method description - calculation according to given formula.
     *
     * @param - from command line (args[0] for variable a; args[1] for variable p;
     *          args[2] for variable m1, args[3] for variable m2).
     */
    public double calculateG(int a, int p, double m1, double m2) {
        if (m1 + m2 != 0) {
            G = 4 * Math.pow(Math.PI, 2) * (Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2)));
            System.out.print(G);
        } else {
            System.out.print("Error - division by zero");
        }
        return G;
    }
}
