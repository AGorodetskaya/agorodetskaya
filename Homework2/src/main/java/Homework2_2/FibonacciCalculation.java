package Homework2_2;

/**
 * @author Anna Gorodetskaya
 * @version 1.0
 * @since 1.0
 */
public class FibonacciCalculation {

    /**
     * method for calculating Fibonacci sequence
     *
     * @param loopType - for choosing loop type (1 - "while", 2 - "do-while", 3 - "for")
     * @param n        - parameter to algorithm
     */
    public void calculateFibonacci(int loopType, int n) {
        int[] fib = new int[n];
        if (loopType < 1 || loopType > 3) {
            System.out.print("Choose 1, 2 or 3 loop type"); // в случае некорректного ввода типа цикла
        } else if (n == 0) {
            System.out.print("Choose number more than 0");
        } else if (n == 1) {
            System.out.print("0");
        } else if (n == 2) {
            System.out.print("0 1");
        } else if (n >= 3) {
            fib[0] = 0;
            fib[1] = 1;
            System.out.print(fib[0] + " " + fib[1] + " ");
            if (loopType == 1) { // цикл while
                int i = 2;
                while (i < n) {
                    fib[i] = fib[i - 2] + fib[i - 1];
                    System.out.print(fib[i] + " ");
                    i++;
                }
            } else if (loopType == 2) { // цикл do-while
                int i = 2;
                do {
                    fib[i] = fib[i - 2] + fib[i - 1];
                    System.out.print(fib[i] + " ");
                    i++;
                }
                while (i < n);
            } else if (loopType == 3) { // цикл for
                for (int i = 2; i < n; i++) {
                    fib[i] = fib[i - 2] + fib[i - 1];
                    System.out.print(fib[i] + " ");
                }
            }
        }
    }
}
