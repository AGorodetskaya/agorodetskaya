package Homework2_2;

/**
 * @author Anna Gorodetskaya
 * @version 1.0
 * @since 1.0
 */
public class FactorialCalculation {

    /**
     * method for calculating Factorial
     *
     * @param loopType - for choosing loop type (1 - "while", 2 - "do-while", 3 - "for")
     * @param n        - parameter to algorithm
     */
    public int calculateFactorial(int loopType, int n) {
        int fact = 1;
        if (n == 0) {
            System.out.print("1"); // для факториала нуля
        } else if (n > 0) {
            if (loopType < 1 || loopType > 3) {
                System.out.print("Choose 1, 2 or 3 loop type "); // в случае некорректного ввода типа цикла
            } else if (loopType == 1) {
                int i = 1;
                while (i <= n) {
                    fact = fact * i;
                    i++;
                }
                System.out.print(fact);
            } else if (loopType == 2) {
                int i = 1;
                do {
                    fact = fact * i;
                    i++;
                } while (i <= n);
                System.out.print(fact);
            } else if (loopType == 3) {
                for (int i = 1; i <= n; i++) {
                    fact = fact * i;
                }
                System.out.print(fact);
            }
        } else {
            System.out.print("Enter number more than 0, calculation possible only for not negative numbers");
        }
        return fact;
    }
}
