package Homework2_2;

/**
 * Homework 2_2
 *
 * @author Anna Gorodetskaya
 * @version 1.0
 * @since 1.0
 */
public class Calculation2 {

    /**
     * Main method description - calculation Fibonacci sequence and factorial
     *
     * @param args - program arguments from command line:
     *             -  args[0] for choosing type of necessary algorithm
     *             (1 for Fibonacci sequence; 2 for factorial calculation);
     *             -  args[1] for choosing loop type
     *             (1 - "while", 2 - "do-while", 3 - "for");
     *             - args [2] - parameter to algorithm
     */
    public static void main(String[] args) {
        String stringAlgorithmId = args[0];
        String stringLoopType = args[1];
        String stringN = args[2];
        int algorithmId = Integer.parseInt(stringAlgorithmId);
        int loopType = Integer.parseInt(stringLoopType);
        int n = Integer.parseInt(stringN);
        if (algorithmId == 1) {
            FibonacciCalculation fibonacciCalculation = new FibonacciCalculation();
            fibonacciCalculation.calculateFibonacci(loopType, n);
        }
        if (algorithmId == 2) {
           FactorialCalculation factorialCalculation = new FactorialCalculation();
           factorialCalculation.calculateFactorial(loopType,n);
        }
        else if (algorithmId < 1 || algorithmId > 2) {
            System.out.print("Print 1 for Fibonacci sequence or 2 for factorial calculation");
        }
    }
}

