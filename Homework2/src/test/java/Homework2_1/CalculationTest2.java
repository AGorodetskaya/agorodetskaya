package Homework2_1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CalculationTest2 {
    private static final double DELTA = 0.1d;
    CalculationG calculationG = new CalculationG();

    @Test
    public void testIncorrectResult() {
        /*Given*/
        int a = 2;
        int p = 3;
        double m1 = -1.0;
        double m2 = 1.0;
        double expected = 11.7d;
        /*When*/
        double actual = calculationG.calculateG(a, p, m1, m2);
        /*Then*/
        Assertions.assertNotEquals(expected, actual, DELTA);
    }
}