package Homework2_2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Calculation2Test {
    FactorialCalculation factorialCalculation = new FactorialCalculation();
    @Test
    public void testFibonacciCalculation() {
        /*Given*/
        int algorithmId = 2;
        int loopType = 3;
        int n = 5;
        int expected = 120;
        /*When*/
        int actual = factorialCalculation.calculateFactorial(loopType,n);
        /*Then*/
        Assertions.assertEquals (expected,actual);
    }
}