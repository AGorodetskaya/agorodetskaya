package Homework2_2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Calculation2Test2 {
    FactorialCalculation factorialCalculation = new FactorialCalculation();
    @Test
    public void testFactorialCalculation() {
        /*Given*/
        int algorithmId = 2;
        int loopType = 0;
        int n = 5;
        int expected = 120;
        /*When*/
        int actual = factorialCalculation.calculateFactorial(loopType,n);
        /*Then*/
        Assertions.assertNotEquals (expected,actual);
    }
}
