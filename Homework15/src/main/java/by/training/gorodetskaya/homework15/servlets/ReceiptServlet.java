package by.training.gorodetskaya.homework15.servlets;

import by.training.gorodetskaya.homework15.services.Bucket;
import by.training.gorodetskaya.homework15.services.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebServlet(asyncSupported = false, name = "Confirm page", urlPatterns = "/confirm")
public class ReceiptServlet extends HttpServlet {

    private Bucket bucket;

    @Override
    public void init() throws ServletException {
        bucket = Bucket.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        PrintWriter writer = resp.getWriter();
        writer.println("<html>" +
                "<body>" +
                "<br><br>" +
                "<h1 align=center>Dear " + session.getAttribute("name") + ", your order" +
                "</h1>" +
                "<br><br>" +
                makeUserBucket(session.getId()) +
                "<br>" +
                "<div align=center>Total: $ " + bucket.countAmount(session.getId()) + "</div>" +
                "</body>" +
                "</html>");
    }

    private String makeUserBucket(String sessionId) {
        List<Product> userBucket = bucket.getUserBucket(sessionId);

        String bucketWithPlaceholders = userBucket.stream()
                .map(product -> "<div align=center>%s) " + product + "</div>")
                .collect(Collectors.joining());

        Object[] objects = Stream.iterate(1, n -> n + 1)
                .limit(userBucket.size())
                .toArray();

        return String.format(bucketWithPlaceholders, objects);
    }
}