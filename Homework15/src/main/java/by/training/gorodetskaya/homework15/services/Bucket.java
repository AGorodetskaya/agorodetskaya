package by.training.gorodetskaya.homework15.services;

import com.google.common.collect.Maps;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class Bucket {

    private static Bucket instance;

    private Map<String, List<Product>> choice = Maps.newHashMap();

    private Bucket() {
    }

    public Map<String, List<Product>> getChoice() {
        return choice;
    }

    public void setChoice(Map<String, List<Product>> choice) {
        this.choice = choice;
    }

    public static synchronized Bucket getInstance() {
        if (instance == null) {
            instance = new Bucket();
        }
        return instance;
    }

    public void addProducts(String sessionId, List<Product> userChoice) {
        choice.put(sessionId, userChoice);
    }

    public List<Product> getUserBucket(String sessionId) {
        return choice.get(sessionId);
    }

    public BigDecimal countAmount(String sessionId) {
        return getUserBucket(sessionId).stream()
                .map(Product::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}