package by.training.gorodetskaya.homework15.servlets;

import by.training.gorodetskaya.homework15.services.Bucket;
import by.training.gorodetskaya.homework15.services.PriceList;
import by.training.gorodetskaya.homework15.services.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(asyncSupported = false, name = "Order page", urlPatterns = "/order")
public class OrderServlet extends HttpServlet {

    private PriceList priceList;
    private Bucket bucket;

    @Override
    public void init() throws ServletException {
        priceList = PriceList.getInstance();
        bucket = Bucket.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        PrintWriter writer = resp.getWriter();
        writer.println("<html>" +
                "<body>" +
                "<br><br>" +
                "<h1 align=center>Hello, " + session.getAttribute("name") + "!" +
                "</h1>" +
                "<br><br>" +
                "<p align=center>Make your order</p>" +
                "<form align=center action=\"/servlet/order\" method=\"post\" name=\"form\">" +
                "<p><select name=\"id\" size=" + priceList.getRange().size() + " multiple required=\"required\">" +
                makeSuggestionList() +
                "</select></p>" +
                "<br>" +
                "<input type=\"submit\" value=\"Submit\">" +
                "</form>" +
                "</body>" +
                "</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] ids = req.getParameterValues("id");
        List<Product> userChoice = Arrays.stream(ids)
                .map(id -> priceList.getRange().get(Integer.parseInt(id)))
                .collect(Collectors.toList());
        HttpSession session = req.getSession();
        bucket.addProducts(session.getId(), userChoice);
        resp.sendRedirect(req.getContextPath() + "/confirm");
    }

    private String makeSuggestionList() {
        return priceList.getRange().entrySet().stream()
                .map(entry -> "<option name=\"id\" value=" + entry.getKey() + ">" + entry.getValue().toString() + "</option>")
                .collect(Collectors.joining());
    }
}