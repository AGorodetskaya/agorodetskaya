package by.training.gorodetskaya.homework15.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(asyncSupported = false, name = "Welcome page", urlPatterns = "/welcome")
public class WelcomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter out = resp.getWriter();
        out.println("<html>" +
                "<body>" +
                "<br><br>" +
                "<h1 align=center>Welcome to Online Shop</h1>" +
                "<br><br>" +
                "<h2 align=center>" +
                "<form action=\"/servlet/welcome\" method=\"post\">" +
                "<input type=\"text\" id=\"name\" name=\"name\" placeholder=\"Enter your name\" required=\"required\">" +
                "<br><br>" +
                "<input type=\"submit\" value=\"Enter\">" +
                "</form>" +
                "</h2>" +
                "</body>" +
                "</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String name = req.getParameter("name");
        session.setAttribute("name", name);
        resp.sendRedirect(req.getContextPath() + "/order");
    }
}