package by.training.gorodetskaya.homework15.services;

import com.google.common.collect.Maps;

import java.math.BigDecimal;
import java.util.Map;

public class PriceList {

    private static PriceList instance;

    private Map<Integer, Product> range = Maps.newHashMap();

    private PriceList() {
    }

    public Map<Integer, Product> getRange() {
        return range;
    }

    public void setRange(Map<Integer, Product> range) {
        this.range = range;
    }

    public static synchronized PriceList getInstance() {
        if (instance == null) {
            instance = new PriceList();
            instance.createRange();
        }
        return instance;
    }

    private void createRange() {
        range.put(1, new Product(1, "book", new BigDecimal("5.50")));
        range.put(2, new Product(2, "pen", new BigDecimal("1.50")));
        range.put(3, new Product(3, "pencil", new BigDecimal("0.80")));
        range.put(4, new Product(4, "notebook", new BigDecimal("4.50")));
    }
}