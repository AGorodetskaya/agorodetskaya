package by.training.gorodetskaya.homework17.servlets;

import by.training.gorodetskaya.homework17.services.BasketService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(asyncSupported = false, name = "Confirm page", urlPatterns = "/confirm")
public class ReceiptServlet extends HttpServlet {

    private static final String NAME = "name";
    private static final String CURRENT_USER_BASKET = "currentUserBasket";
    private static final String TOTAL_AMOUNT = "totalAmount";
    private static final String RECEIPT_JSP = "WEB-INF/views/receipt.jsp";
    private final BasketService basketService = BasketService.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        req.setAttribute(CURRENT_USER_BASKET, basketService.getUserOrder((String) session.getAttribute(NAME)));
        req.setAttribute(TOTAL_AMOUNT, basketService.countAmount((String) session.getAttribute(NAME)));

        req.getRequestDispatcher(RECEIPT_JSP).forward(req, resp);
    }
}