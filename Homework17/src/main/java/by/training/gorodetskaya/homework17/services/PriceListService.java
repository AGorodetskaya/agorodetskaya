package by.training.gorodetskaya.homework17.services;

import by.training.gorodetskaya.homework17.models.Product;
import com.google.common.collect.Maps;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

public class PriceListService {

    private static PriceListService instance;

    private Map<Integer, Product> productMap = Maps.newHashMap();

    private PriceListService() {
    }

    public Map<Integer, Product> getProductMap() {
        return productMap;
    }

    public void setProductMap(Map<Integer, Product> productMap) {
        this.productMap = productMap;
    }

    public static synchronized PriceListService getInstance() {
        if (instance == null) {
            instance = new PriceListService();
            instance.createRange();
        }
        return instance;
    }

    private void createRange() {
        productMap.put(1, new Product(1, "book", new BigDecimal("5.50")));
        productMap.put(2, new Product(2, "pen", new BigDecimal("1.50")));
        productMap.put(3, new Product(3, "pencil", new BigDecimal("0.80")));
        productMap.put(4, new Product(4, "notebook", new BigDecimal("4.50")));
    }

    public Collection<Product> getRange() {
        return productMap.values();
    }
}