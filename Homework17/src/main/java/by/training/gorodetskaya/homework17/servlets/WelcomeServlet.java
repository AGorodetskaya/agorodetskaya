package by.training.gorodetskaya.homework17.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(asyncSupported = false, name = "Welcome page", urlPatterns = "/welcome")
public class WelcomeServlet extends HttpServlet {

    private static final String NAME = "name";
    private static final String AGREEMENT = "agreement";
    private static final String ORDER_PAGE = "/order";
    private static final String WELCOME_JSP = "WEB-INF/views/welcome.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(WELCOME_JSP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String name = req.getParameter(NAME);
        String agreement = req.getParameter(AGREEMENT);
        session.setAttribute(NAME, name);
        session.setAttribute(AGREEMENT, agreement);

        resp.sendRedirect(req.getContextPath() + ORDER_PAGE);
    }
}