package by.training.gorodetskaya.homework17.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(asyncSupported = false, name = "Error page", urlPatterns = "/error")
public class ErrorServlet extends HttpServlet {

    private static final String ERROR_JSP = "WEB-INF/views/error.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(ERROR_JSP).forward(req, resp);
    }
}