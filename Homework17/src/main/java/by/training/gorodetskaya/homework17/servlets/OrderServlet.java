package by.training.gorodetskaya.homework17.servlets;

import by.training.gorodetskaya.homework17.models.Product;
import by.training.gorodetskaya.homework17.services.BasketService;
import by.training.gorodetskaya.homework17.services.PriceListService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(asyncSupported = false, name = "Order page", urlPatterns = "/order")
public class OrderServlet extends HttpServlet {

    private static final String NAME = "name";
    private static final String ID = "id";
    private static final String ORDER_PAGE = "/order";
    private static final String ORDER_JSP = "WEB-INF/views/order.jsp";
    private static final String RANGE = "range";
    private static final String CURRENT_USER_BASKET = "currentUserBasket";
    private final PriceListService priceList = PriceListService.getInstance();
    private final BasketService basketService = BasketService.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        req.setAttribute(RANGE, priceList.getRange());
        req.setAttribute(CURRENT_USER_BASKET, basketService.getUserOrder((String) session.getAttribute(NAME)));

        req.getRequestDispatcher(ORDER_JSP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        String id = request.getParameter(ID);
        Product product = priceList.getProductMap().get(Integer.parseInt(id));
        HttpSession session = request.getSession();
        basketService.addProduct((String) session.getAttribute(NAME), product);

        resp.sendRedirect(request.getContextPath() + ORDER_PAGE);
    }
}