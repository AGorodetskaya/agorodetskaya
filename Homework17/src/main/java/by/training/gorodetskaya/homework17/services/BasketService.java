package by.training.gorodetskaya.homework17.services;

import by.training.gorodetskaya.homework17.models.Product;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.math.BigDecimal;
import java.util.Collection;

public class BasketService {

    private static BasketService instance;

    private Multimap<String, Product> allUsersBasket = ArrayListMultimap.create();

    private BasketService() {
    }

    public Multimap<String, Product> getAllUsersBasket() {
        return allUsersBasket;
    }

    public void setAllUsersBasket(Multimap<String, Product> allUsersBasket) {
        this.allUsersBasket = allUsersBasket;
    }

    public static synchronized BasketService getInstance() {
        if (instance == null) {
            instance = new BasketService();
        }
        return instance;
    }

    public void addProduct(String userName, Product product) {
        getAllUsersBasket().put(userName, product);
    }

    public Collection<Product> getUserOrder(String userName) {
        return getAllUsersBasket().get(userName);
    }

    public BigDecimal countAmount(String userName) {
        return getAllUsersBasket().get(userName).stream()
                .map(Product::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}