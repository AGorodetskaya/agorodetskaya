package by.training.gorodetskaya.homework8.task2;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class TenderCalculatorTest {
    private TenderCalculator tenderCalculator;
    private Map<Worker, Integer> brigadeTenderList;
    private List<Worker> brigadeSharashMontazh;
    private List<Worker> brigadeStroybat;
    private List<Worker> brigadeMoguchayaKuchka;
    private List<List<Worker>> listBrigades;

    @Before
    public void initiate() {
        tenderCalculator = new TenderCalculator();
        brigadeTenderList = new HashMap<>();
        listBrigades = new ArrayList<>();

        brigadeSharashMontazh = createSharashMontazhBrigade();
        brigadeStroybat = createStroybatBrigade();
        brigadeMoguchayaKuchka = createMoguzhayaKuchkaBrigade();
        brigadeTenderList = createBrigadeTenderList();

        listBrigades.add(brigadeSharashMontazh);
        listBrigades.add(brigadeStroybat);
        listBrigades.add(brigadeMoguchayaKuchka);
    }

    private List<Worker> createSharashMontazhBrigade() {
        brigadeSharashMontazh = new ArrayList<>();
        brigadeSharashMontazh.add(new Worker(Collections.singleton(Skill.PAINTER), 450.0));
        brigadeSharashMontazh.add(new Worker(Collections.singleton(Skill.BUILDER), 400.0));
        brigadeSharashMontazh.add(new Worker(Collections.singleton(Skill.BUILDER), 400.0));
        brigadeSharashMontazh.add(new Worker(Collections.singleton(Skill.PLASTERER), 370.0));
        brigadeSharashMontazh.add(new Worker(Collections.singleton(Skill.PLASTERER), 370.0));
        return brigadeSharashMontazh;
    }

    private List<Worker> createStroybatBrigade() {
        brigadeStroybat = new ArrayList<>();
        brigadeStroybat.add(new Worker(Collections.singleton(Skill.BUILDER), 450.0));
        brigadeStroybat.add(new Worker(Collections.singleton(Skill.BUILDER), 470.0));
        brigadeStroybat.add(new Worker(Collections.singleton(Skill.BUILDER), 470.0));
        brigadeStroybat.add(new Worker(Collections.singleton(Skill.PAINTER), 200.0));
        brigadeStroybat.add(new Worker(Collections.singleton(Skill.PLASTERER), 200.0));
        brigadeStroybat.add(new Worker(Sets.newHashSet(Skill.PLASTERER, Skill.PAINTER), 500.0));
        return brigadeStroybat;
    }

    private List<Worker> createMoguzhayaKuchkaBrigade() {
        brigadeMoguchayaKuchka = new ArrayList<>();
        brigadeMoguchayaKuchka.add(new Worker(Collections.singleton(Skill.PAINTER), 560.0));
        brigadeMoguchayaKuchka.add(new Worker(Collections.singleton(Skill.PAINTER), 420.0));
        brigadeMoguchayaKuchka.add(new Worker(Collections.singleton(Skill.PAINTER), 540.0));
        brigadeMoguchayaKuchka.add(new Worker(Collections.singleton(Skill.BUILDER), 500.0));
        brigadeMoguchayaKuchka.add(new Worker(Collections.singleton(Skill.PLASTERER), 540.0));
        return brigadeMoguchayaKuchka;
    }

    private Map<Worker, Integer> createBrigadeTenderList() {
        brigadeTenderList = new HashMap<>();
        brigadeTenderList.put(new Worker(Collections.singleton(Skill.BUILDER)), 2);
        brigadeTenderList.put(new Worker(Collections.singleton(Skill.PLASTERER)), 1);
        brigadeTenderList.put(new Worker(Collections.singleton(Skill.PAINTER)), 1);
        return brigadeTenderList;
    }

    @Test
    public void makeTenderTest() {
        List<Worker> expected = brigadeSharashMontazh;
        List<Worker> actual = tenderCalculator.makeTender(listBrigades, brigadeTenderList);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void makeTenderTestWithMultiWorker() {
        brigadeTenderList.put(new Worker(Sets.newHashSet(Skill.PLASTERER, Skill.PAINTER)), 1);
        List<Worker> expected = brigadeStroybat;
        List<Worker> actual = tenderCalculator.makeTender(listBrigades, brigadeTenderList);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = OutOfSuitableBrigadeException.class)
    public void makeClosedTenderTest() throws OutOfSuitableBrigadeException {
        brigadeTenderList.put(new Worker(Collections.singleton(Skill.BUILDER)),5);
        tenderCalculator.makeTender(listBrigades, brigadeTenderList);
    }
}