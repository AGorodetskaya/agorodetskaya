package by.training.gorodetskaya.homework8.task2;

public class OutOfSuitableBrigadeException extends RuntimeException {
    public OutOfSuitableBrigadeException(String message) {
        super(message);
    }
}