package by.training.gorodetskaya.homework8.task2;

public enum Skill {
    BUILDER, PAINTER, PLASTERER;
}