package by.training.gorodetskaya.homework8.task2;

import java.util.*;

public class TenderCalculator {

    public List<Worker> makeTender(List<List<Worker>> brigades, Map<Worker, Integer> brigadeTenderList) {
        List<List<Worker>> suitableBrigades = findSuitableBrigades(brigades, brigadeTenderList);
        if (suitableBrigades.isEmpty()) {
            throw new OutOfSuitableBrigadeException("Tender will be finished");
        } else if (suitableBrigades.size() == 1) {
            return suitableBrigades.get(0);
        } else {
            return chooseSuitableBrigade(suitableBrigades);
        }
    }

    private List<List<Worker>> findSuitableBrigades(List<List<Worker>> brigades, Map<Worker, Integer> brigadeTenderList) {
        List<List<Worker>> suitableBrigades = new ArrayList<>();
        for (List<Worker> brigade : brigades) {
            if (isSuitableBrigades(brigade, brigadeTenderList)) {
                suitableBrigades.add(brigade);
            }
        }
        return suitableBrigades;
    }

    private boolean isSuitableBrigades(List<Worker> brigade, Map<Worker, Integer> brigadeTenderList) {
        for (Map.Entry<Worker, Integer> entry : brigadeTenderList.entrySet()) {
            int number = Collections.frequency(brigade, entry.getKey());
            if (number < entry.getValue()) {
                return false;
            }
        }
        return true;
    }

    private Double calculateBrigadeCost(List<Worker> brigade) {
        double brigadeCost = 0.0;
        for (Worker worker : brigade) {
            brigadeCost += worker.getSalary();
        }
        return brigadeCost;
    }

    private List<Worker> chooseSuitableBrigade(List<List<Worker>> suitableBrigades) {
        TreeMap<Double, List<Worker>> mapOfBrigades = new TreeMap<>();
        for (List<Worker> brigade : suitableBrigades) {
            mapOfBrigades.put(calculateBrigadeCost(brigade), brigade);
        }
        return mapOfBrigades.firstEntry().getValue();
    }
}