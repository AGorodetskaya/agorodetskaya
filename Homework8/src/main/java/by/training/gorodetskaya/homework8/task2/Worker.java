package by.training.gorodetskaya.homework8.task2;

import java.util.Objects;
import java.util.Set;

public class Worker {

    private Set<Skill> workerSkills;
    private double salary;

    public Set<Skill> getWorkerSkills() {
        return workerSkills;
    }

    public void setWorkerSkills(Set<Skill> workerSkills) {
        this.workerSkills = workerSkills;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Worker(Set<Skill> workerSkills, double salary) {
        this.workerSkills = workerSkills;
        this.salary = salary;
    }

    public Worker(Set<Skill> workerSkills) {
        this.workerSkills = workerSkills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Worker worker = (Worker) o;
        return Objects.equals(workerSkills, worker.workerSkills);
    }

    @Override
    public int hashCode() {
        return Objects.hash(workerSkills);
    }
}