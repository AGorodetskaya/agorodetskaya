package by.training.gorodetskaya.homework8.task1;

public class IndexOutOfBoundsCustomException extends RuntimeException {
    public IndexOutOfBoundsCustomException(String message) {
        super(message);
    }
}
