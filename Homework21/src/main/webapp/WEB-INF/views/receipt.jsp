<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<body>
<br><br>
<h1 align=center> Dear <security:authentication property="principal.username"/>, your order</h1>
<br><br>
<c:forEach var="product" items="${currentUserBasket}" varStatus = "loop">
       <div align=center> ${loop.index+1}) ${product} </div>
<br>
</c:forEach>
<br>
<div align=center>Total: $ ${totalAmount} </div>
<br><br>
<form align=center action="logout" method="get" name="logout">
<input type="submit" value="Logout">
</body>
</html>