<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<body>
<br><br>
<h1 align=center>Welcome to Online Shop</h1>
<br><br>
<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION.message}">
  <div align=center class="error">
    <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />
  </div>
</c:if>
<br><br>
<div align=center>
<form action= "welcome" method="post">
<input type="text" id="login" name="login" placeholder="Enter your name" required="required">
<input type="password" id="password" name="password" placeholder="Enter your password" required="required">
<br><br>
<input type="submit" value="Enter">
</form>
<br><br>
<form action= "register" method="get">
<div>If you are not registered yet, please, register at first</div>
<br>
<input type="submit" id="registration" value="Register">
<br><br>
</form>
</div>
</body>
</html>