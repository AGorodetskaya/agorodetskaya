package by.training.gorodetskaya.homework21.services.impl;

import by.training.gorodetskaya.homework21.converters.UserConverter;
import by.training.gorodetskaya.homework21.dto.UserDto;
import by.training.gorodetskaya.homework21.entity.enums.Role;
import by.training.gorodetskaya.homework21.dao.api.UserDao;
import by.training.gorodetskaya.homework21.entity.User;
import by.training.gorodetskaya.homework21.services.api.OrderService;
import by.training.gorodetskaya.homework21.services.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final OrderService orderService;
    private final UserConverter userConverter;

    public UserServiceImpl(UserDao userDao, OrderService orderService, UserConverter userConverter) {
        this.userDao = userDao;
        this.userConverter = userConverter;
        this.orderService = orderService;
    }

    public User addUser(UserDto userDto) {
        userDto.setRole(Role.USER);
        User user = userConverter.toEntity(userDto);
        user = userDao.addUser(user);
        orderService.addOrder(user.getId());
        return user;
    }
}