package by.training.gorodetskaya.homework21.services.api;

import by.training.gorodetskaya.homework21.entity.Good;
import by.training.gorodetskaya.homework21.entity.Order;

import java.util.List;

public interface OrderService {

    void addOrder(int userId);

    Order getOrderByUserName(String username);

    void updateAmountOfOrder(String userName);

    List<Good> getUserGoods(String userName);

    void addGoodInOrder(Order order, Good good);
}