package by.training.gorodetskaya.homework21.dto;

import by.training.gorodetskaya.homework21.entity.enums.Role;

public class UserDto {

    private int id;
    private String login;
    private String password;
    private Role role;

    public UserDto(int id, String login, String password, Role role) {
    }

    public UserDto(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public UserDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
