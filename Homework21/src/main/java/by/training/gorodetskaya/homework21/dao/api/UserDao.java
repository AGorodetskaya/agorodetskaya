package by.training.gorodetskaya.homework21.dao.api;

import by.training.gorodetskaya.homework21.entity.User;

import java.util.Optional;

public interface UserDao {

    User addUser(User user);

    Optional<User> getUserByName(String name);
}