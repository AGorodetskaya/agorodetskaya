package by.training.gorodetskaya.homework21.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@ComponentScan("by.training.gorodetskaya.homework21.controllers")
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    private static final String PREFIX = "/WEB-INF/views/";
    private static final String SUFFIX = ".jsp";
    private static final String ERROR = "/error";
    private static final String ERROR_JSP = "error";

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix(PREFIX);
        resolver.setSuffix(SUFFIX);
        resolver.setViewClass(JstlView.class);
        registry.viewResolver(resolver);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController(ERROR).setViewName(ERROR_JSP);
        registry.addViewController("/welcome").setViewName("welcome");
    }
}