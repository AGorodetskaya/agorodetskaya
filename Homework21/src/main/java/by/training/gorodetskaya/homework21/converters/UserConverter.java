package by.training.gorodetskaya.homework21.converters;

import by.training.gorodetskaya.homework21.dto.UserDto;
import by.training.gorodetskaya.homework21.entity.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    final private PasswordEncoder passwordEncoder;

    public UserConverter(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public User toEntity(final UserDto userDto) {
        return new User(userDto.getLogin(), passwordEncoder.encode(userDto.getPassword()), userDto.getRole());
    }

    public UserDto toDto(final User user) {
        return new UserDto(user.getId(), user.getLogin(), "", user.getRole());
    }

    public User enrich(final User user, final UserDto userDto) {
        user.setLogin(userDto.getLogin());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setRole(userDto.getRole());
        return user;
    }
}