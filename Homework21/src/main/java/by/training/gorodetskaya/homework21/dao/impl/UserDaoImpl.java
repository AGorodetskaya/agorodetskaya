package by.training.gorodetskaya.homework21.dao.impl;

import by.training.gorodetskaya.homework21.config.DatabaseConnector;
import by.training.gorodetskaya.homework21.dao.api.UserDao;
import by.training.gorodetskaya.homework21.entity.User;
import by.training.gorodetskaya.homework21.entity.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Repository
public class UserDaoImpl implements UserDao {

    private static final String ADD_USER = "INSERT INTO USER (LOGIN, PASSWORD, ROLE) VALUES (?, ?, ?)";
    private static final String SELECT_USER_BY_NAME = "SELECT ID, LOGIN, PASSWORD FROM USER WHERE LOGIN=?";
    private static final String ID = "ID";
    private static final String LOGIN = "LOGIN";
    private static final String PASSWORD = "PASSWORD";

    private final DatabaseConnector connector;

    public UserDaoImpl(DatabaseConnector connector) {
        this.connector = connector;
    }

    @Override
    public User addUser(User user) {

        try (Connection connection = connector.getConnection();
             PreparedStatement preparedStatementInsert = connection.prepareStatement(ADD_USER);
             PreparedStatement preparedStatementSelect = connection.prepareStatement(SELECT_USER_BY_NAME)) {

            preparedStatementInsert.setString(1, user.getLogin());
            preparedStatementInsert.setString(2, user.getPassword());
            preparedStatementInsert.setString(3, user.getRole().name());
            preparedStatementInsert.executeUpdate();

            preparedStatementSelect.setString(1, user.getLogin());
            ResultSet resultSet = preparedStatementSelect.executeQuery();

            while (resultSet.next()) {
                user.setId(resultSet.getInt(ID));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return user;
    }

    @Override
    public Optional<User> getUserByName(String name) {
        User user = null;
        try (Connection connection = connector.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_NAME)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt(ID));
                user.setLogin(resultSet.getString(LOGIN));;
                user.setPassword(resultSet.getString(PASSWORD));;
                user.setRole(Role.USER);;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.ofNullable(user);
    }
}