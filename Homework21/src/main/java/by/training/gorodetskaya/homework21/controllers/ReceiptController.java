package by.training.gorodetskaya.homework21.controllers;

import by.training.gorodetskaya.homework21.authorization.CustomerUserPrincipal;
import by.training.gorodetskaya.homework21.entity.User;
import by.training.gorodetskaya.homework21.services.api.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/receipt")
public class ReceiptController {

    private static final String USER_BASKET = "currentUserBasket";
    private static final String TOTAL_AMOUNT = "totalAmount";
    private static final String RECEIPT_JSP = "receipt";

    private final OrderService orderService;

    public ReceiptController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    public ModelAndView confirmOrder(@AuthenticationPrincipal CustomerUserPrincipal customerUserPrincipal) {

        User user = customerUserPrincipal.getUser();

        final ModelAndView modelAndView = new ModelAndView(RECEIPT_JSP);
        modelAndView.addObject(USER_BASKET, orderService.getUserGoods(user.getLogin()));
        modelAndView.addObject(TOTAL_AMOUNT, orderService.getOrderByUserName(user.getLogin()).getTotalPrice());

        return modelAndView;
    }
}