package by.training.gorodetskaya.homework21.controllers;

import by.training.gorodetskaya.homework21.dto.UserDto;
import by.training.gorodetskaya.homework21.services.api.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Objects;

@Controller
@RequestMapping("/register")
public class RegistrationController {

    private static final String REDIRECT_WELCOME = "redirect:welcome";
    private static final String REDIRECT_ERROR = "redirect:error";

    private final UserService userService;

    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String showRegistrationPage() {
        return "register";
    }

    @PostMapping
    public ModelAndView saveUser(@ModelAttribute("userDto") UserDto userDto,
                                 @RequestParam(value = "agreement", required = false) Boolean isAgree) {

        if (Objects.nonNull(isAgree)) {
            userService.addUser(userDto);
            return new ModelAndView(REDIRECT_WELCOME);
        } else {
            return new ModelAndView(REDIRECT_ERROR);
        }
    }
}