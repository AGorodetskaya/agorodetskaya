package by.training.gorodetskaya.homework21.services.api;

import by.training.gorodetskaya.homework21.dto.UserDto;
import by.training.gorodetskaya.homework21.entity.User;

public interface UserService {

    User addUser(UserDto userDto);

}