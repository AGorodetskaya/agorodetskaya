package by.training.gorodetskaya.homework21.services.impl;

import by.training.gorodetskaya.homework21.authorization.CustomerUserPrincipal;
import by.training.gorodetskaya.homework21.dao.api.UserDao;
import by.training.gorodetskaya.homework21.entity.User;
import by.training.gorodetskaya.homework21.entity.enums.Role;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserDao userDao;

    public CustomUserDetailsService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(final String login) throws UsernameNotFoundException {
        final User user = userDao.getUserByName(login).orElseThrow(() -> new UsernameNotFoundException("User with login "
                + login + " was not found"));
        return new CustomerUserPrincipal(user);
    }
}