package by.training.gorodetskaya.homework21.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"by.training.gorodetskaya.homework21.config", "by.training.gorodetskaya.homework21.dao",
        "by.training.gorodetskaya.homework21.services", "by.training.gorodetskaya.homework21.converters"})
public class RootConfig {
}