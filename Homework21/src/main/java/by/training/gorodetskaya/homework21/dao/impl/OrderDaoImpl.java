package by.training.gorodetskaya.homework21.dao.impl;

import by.training.gorodetskaya.homework21.config.DatabaseConnector;
import by.training.gorodetskaya.homework21.dao.api.OrderDao;
import by.training.gorodetskaya.homework21.entity.Good;
import by.training.gorodetskaya.homework21.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderDaoImpl implements OrderDao {

    private static final String INSERT_NEW_ORDER = "INSERT INTO \"ORDER\" (USER_ID, TOTAL_PRICE) VALUES (?, ?)";
    private static final String SELECT_USER_BY_ID = "SELECT ID, USER_ID, TOTAL_PRICE FROM \"ORDER\" WHERE USER_ID=?";
    private static final String NULL = "0.00";
    private static final String GOOD_ID = "ID";
    private static final String USER_ID = "USER_ID";
    private static final String TOTAL_PRICE = "TOTAL_PRICE";
    private static final String COUNT_AMOUNT = "SELECT SUM(GOOD.PRICE), \"ORDER\".ID FROM GOOD " +
            "JOIN ORDER_GOOD ON GOOD.ID = ORDER_GOOD.GOOD_ID " +
            "JOIN \"ORDER\" ON ORDER_GOOD.ORDER_ID = \"ORDER\".ID " +
            "JOIN USER ON USER.ID = \"ORDER\".USER_ID " +
            "WHERE USER.LOGIN = ?";
    private static final String UPDATE_TOTAL_PRICE = "UPDATE \"ORDER\" SET TOTAL_PRICE=? WHERE \"ORDER\".ID=?";
    private static final String INSERT_GOOD_IN_ORDER = "INSERT INTO ORDER_GOOD (ORDER_ID, GOOD_ID) VALUES (?, ?)";
    private static final String SELECT_USER_GOODS = "SELECT GOOD.ID, GOOD.TITLE, GOOD.PRICE FROM GOOD " +
            "JOIN ORDER_GOOD ON GOOD.ID = ORDER_GOOD.GOOD_ID " +
            "JOIN \"ORDER\" ON ORDER_GOOD.ORDER_ID = \"ORDER\".ID " +
            "JOIN USER ON USER.ID = \"ORDER\".USER_ID " +
            "WHERE USER.LOGIN = ?";
    private static final String TITLE = "TITLE";
    private static final String PRICE = "PRICE";
    private static final String SELECT_USER_BY_NAME = "SELECT \"ORDER\".ID, USER_ID, TOTAL_PRICE FROM" +
            " \"ORDER\" JOIN USER ON \"ORDER\".USER_ID=USER.ID WHERE LOGIN=?";

    private final DatabaseConnector connector;

    public OrderDaoImpl(DatabaseConnector connector) {
        this.connector = connector;
    }

    @Override
    public void addOrder(int userId) {
        try (Connection connection = connector.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_ORDER)) {
            preparedStatement.setInt(1, userId);
            preparedStatement.setBigDecimal(2, new BigDecimal(NULL));
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Order getOrderByUserId(int id) {
        Order order = new Order();
        try (Connection connection = connector.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                order.setId(resultSet.getInt(GOOD_ID));
                order.setUserId(resultSet.getInt(USER_ID));
                order.setTotalPrice(resultSet.getBigDecimal(TOTAL_PRICE));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return order;
    }

    @Override
    public Order getOrderByUserName(String username) {
        Order order = new Order();
        try (Connection connection = connector.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_NAME)) {
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                order.setId(resultSet.getInt(GOOD_ID));
                order.setUserId(resultSet.getInt(USER_ID));
                order.setTotalPrice(resultSet.getBigDecimal(TOTAL_PRICE));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return order;
    }

    @Override
    public void updateAmountOfOrder(String username) {

        BigDecimal amount = null;
        Integer orderId = null;

        try (Connection connection = connector.getConnection();
             PreparedStatement preparedStatementUpdate = connection.prepareStatement(UPDATE_TOTAL_PRICE);
             PreparedStatement preparedStatementCountAmount = connection.prepareStatement(COUNT_AMOUNT)) {

            preparedStatementCountAmount.setString(1, username);
            ResultSet resultSet = preparedStatementCountAmount.executeQuery();
            while (resultSet.next()) {
                amount = resultSet.getBigDecimal(1);
                orderId = resultSet.getInt(2);
            }

            preparedStatementUpdate.setBigDecimal(1, amount);
            preparedStatementUpdate.setInt(2, orderId);
            preparedStatementUpdate.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void addGoodInOrder(Order order, Good good) {
        try (Connection connection = connector.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_GOOD_IN_ORDER)) {
            preparedStatement.setInt(1, order.getId());
            preparedStatement.setInt(2, good.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Good> getUserGoods(String username) {
        List<Good> list = new ArrayList<>();
        try (Connection connection = connector.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_GOODS)) {
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Good good = new Good();
                good.setId(resultSet.getInt(GOOD_ID));
                good.setTitle(resultSet.getString(TITLE));
                good.setPrice(resultSet.getBigDecimal(PRICE));
                list.add(good);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }
}