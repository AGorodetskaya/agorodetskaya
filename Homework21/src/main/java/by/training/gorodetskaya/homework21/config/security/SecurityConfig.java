package by.training.gorodetskaya.homework21.config.security;

import by.training.gorodetskaya.homework21.entity.enums.Role;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().disable()
                .csrf().disable()

                .authorizeRequests()
                .antMatchers("/welcome", "/register", "/error", "/404")
                .permitAll()

                .and()
                .authorizeRequests()
                .antMatchers("/receipt", "/order")
                .hasAuthority(Role.USER.name())
                .anyRequest().authenticated()

                .and()
                .formLogin()
                .loginProcessingUrl("/welcome")
                .loginPage("/welcome")
                .failureUrl("/welcome")
                .defaultSuccessUrl("/order")
                .permitAll()
                .usernameParameter("login")
                .passwordParameter("password")
                .and()
                .logout().permitAll();
    }
}