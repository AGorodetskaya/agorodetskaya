package by.training.gorodetskaya.homework21.controllers;

import by.training.gorodetskaya.homework21.authorization.CustomerUserPrincipal;
import by.training.gorodetskaya.homework21.entity.Good;
import by.training.gorodetskaya.homework21.entity.Order;
import by.training.gorodetskaya.homework21.entity.User;
import by.training.gorodetskaya.homework21.services.api.GoodService;
import by.training.gorodetskaya.homework21.services.api.OrderService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/order")
public class OrderController {

    private static final String RANGE = "range";
    private static final String USER_BASKET = "currentUserBasket";
    private static final String ORDER_JSP = "order";
    private static final String REDIRECT_ORDER = "redirect:order";

    private final GoodService goodService;
    private final OrderService orderService;

    public OrderController(GoodService goodService, OrderService orderService) {
        this.goodService = goodService;
        this.orderService = orderService;
    }

    @GetMapping
    public ModelAndView getUserGoods(@AuthenticationPrincipal CustomerUserPrincipal customerUserPrincipal) {
        User user = customerUserPrincipal.getUser();

        final ModelAndView modelAndView = new ModelAndView(ORDER_JSP);
        modelAndView.addObject(RANGE, goodService.getAllGoods());
        modelAndView.addObject(USER_BASKET, orderService.getUserGoods(user.getLogin()));

        return modelAndView;
    }

    @PostMapping
    public ModelAndView addGoodIntoBasket(@AuthenticationPrincipal CustomerUserPrincipal customerUserPrincipal,
                                          @RequestParam("id") int id) {
        User user = customerUserPrincipal.getUser();
        Good good = goodService.getGoodById(id);
        Order order = orderService.getOrderByUserName(user.getLogin());
        orderService.addGoodInOrder(order, good);
        orderService.updateAmountOfOrder(user.getLogin());

        return new ModelAndView(REDIRECT_ORDER);
    }
}