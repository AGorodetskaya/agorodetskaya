package by.training.gorodetskaya.homework21.services.impl;

import by.training.gorodetskaya.homework21.dao.api.GoodDao;
import by.training.gorodetskaya.homework21.entity.Good;
import by.training.gorodetskaya.homework21.services.api.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class GoodServiceImpl implements GoodService {

    private final GoodDao goodDao;

    public GoodServiceImpl(GoodDao goodDao) {
        this.goodDao = goodDao;
    }

    public Good getGoodById(int id) {
        return goodDao.getGoodById(id);
    }

    public Set<Good> getAllGoods() {
        return goodDao.getAllGoods();
    }
}