package by.training.gorodetskaya.homework21.services.impl;

import by.training.gorodetskaya.homework21.dao.api.OrderDao;
import by.training.gorodetskaya.homework21.entity.Good;
import by.training.gorodetskaya.homework21.entity.Order;
import by.training.gorodetskaya.homework21.services.api.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderDao orderDao;

    public OrderServiceImpl(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    public void addOrder(int userId) {
        orderDao.addOrder(userId);
    }

    public Order getOrderByUserName(String username) {
        return orderDao.getOrderByUserName(username);
    }

    public void updateAmountOfOrder(String userName) {
        orderDao.updateAmountOfOrder(userName);
    }

    public List<Good> getUserGoods(String userName) {
        return orderDao.getUserGoods(userName);
    }

    public void addGoodInOrder(Order order, Good good) {
        orderDao.addGoodInOrder(order, good);
    }
}