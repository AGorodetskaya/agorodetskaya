package by.training.gorodetskaya.task1.runnable;

import by.training.gorodetskaya.task1.ATMDeposit;

import java.util.Random;

public class AddingTask implements Runnable {

    private ATMDeposit ATMDeposit;

    public AddingTask(ATMDeposit ATMDeposit) {
        this.ATMDeposit = ATMDeposit;
    }

    public ATMDeposit getATMDeposit() {
        return ATMDeposit;
    }

    public void setATMDeposit(ATMDeposit ATMDeposit) {
        this.ATMDeposit = ATMDeposit;
    }

    @Override
    public void run() {
        boolean needMakeOperation = true;
        while (needMakeOperation) {
            needMakeOperation = ATMDeposit.add(new Random().nextInt(15) + 5);
        }
        if (!needMakeOperation) {
            System.out.println(Thread.currentThread().getName() + " is finished");
        }
    }
}