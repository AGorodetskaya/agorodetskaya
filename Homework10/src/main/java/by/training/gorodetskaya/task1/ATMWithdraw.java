package by.training.gorodetskaya.task1;

public class ATMWithdraw {

    private Card card;

    public ATMWithdraw(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public boolean withdraw(int amountForWithdrawing) {
        return card.withdraw(amountForWithdrawing);
    }
}