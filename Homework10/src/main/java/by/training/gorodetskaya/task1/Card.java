package by.training.gorodetskaya.task1;

public class Card {

    private int balance;

    public Card(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public boolean add(int amountForAdding) {
        synchronized (this) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (balance < 1000 && balance > 0) {
                balance = balance + amountForAdding;
                System.out.println("Balance was increased on " + amountForAdding + ". Now your balance is " + balance + " " + Thread.currentThread().getName());
                return true;
            }
            return false;
        }
    }

    public boolean withdraw(int amountForWithdrawing) {
        synchronized (this) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (balance < 1000 && balance > 0) {
                balance = balance - amountForWithdrawing;
                System.out.println("Balance was decreased with " + amountForWithdrawing + ". Now your balance is " + getBalance() + " " + Thread.currentThread().getName());
                return true;
            }
        }
        return false;
    }
}
