package by.training.gorodetskaya.task1;

import by.training.gorodetskaya.task1.runnable.AddingTask;
import by.training.gorodetskaya.task1.runnable.WithdrawingTask;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Card card = new Card(500);
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(8);
        executorService.scheduleWithFixedDelay(new AddingTask(new ATMDeposit(card)), 1, 1, SECONDS);
        executorService.scheduleWithFixedDelay(new AddingTask(new ATMDeposit(card)), 1, 1, SECONDS);
        executorService.scheduleWithFixedDelay(new AddingTask(new ATMDeposit(card)), 1, 1, SECONDS);
        executorService.scheduleWithFixedDelay(new AddingTask(new ATMDeposit(card)), 1, 1, SECONDS);
        executorService.scheduleWithFixedDelay(new WithdrawingTask(new ATMWithdraw(card)), 1, 1, SECONDS);
        executorService.scheduleWithFixedDelay(new WithdrawingTask(new ATMWithdraw(card)), 1, 1, SECONDS);
        executorService.scheduleWithFixedDelay(new WithdrawingTask(new ATMWithdraw(card)), 1, 1, SECONDS);
        executorService.scheduleWithFixedDelay(new WithdrawingTask(new ATMWithdraw(card)), 1, 1, SECONDS);
        Thread.sleep(2000);
        executorService.shutdown();
        executorService.awaitTermination(1, SECONDS);
    }
}