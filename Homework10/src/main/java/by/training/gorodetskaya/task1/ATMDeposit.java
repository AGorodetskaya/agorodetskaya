package by.training.gorodetskaya.task1;

public class ATMDeposit {

    private Card card;

    public ATMDeposit(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public boolean add(int amountForAdding) {
        return card.add(amountForAdding);
    }
}