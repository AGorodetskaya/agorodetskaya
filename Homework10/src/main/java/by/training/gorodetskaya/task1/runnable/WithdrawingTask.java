package by.training.gorodetskaya.task1.runnable;

import by.training.gorodetskaya.task1.ATMWithdraw;

import java.util.Random;

public class WithdrawingTask implements Runnable {

    private by.training.gorodetskaya.task1.ATMWithdraw ATMWithdraw;

    public WithdrawingTask(ATMWithdraw ATMWithdraw) {
        this.ATMWithdraw = ATMWithdraw;
    }

    public ATMWithdraw getATMWithdraw() {
        return ATMWithdraw;
    }

    public void setATMWithdraw(ATMWithdraw ATMWithdraw) {
        this.ATMWithdraw = ATMWithdraw;
    }

    @Override
    public void run() {
        boolean needMakeOperation = true;
        while (needMakeOperation) {
            needMakeOperation = ATMWithdraw.withdraw(new Random().nextInt(15) + 15);
        }
        if (!needMakeOperation) {
            System.out.println(Thread.currentThread().getName() + " is finished");
        }
    }
}