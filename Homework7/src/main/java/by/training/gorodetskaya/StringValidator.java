package by.training.gorodetskaya;

public class StringValidator implements Validator<String> {

    public final static String REGEX = "^[A-Z].*";

    public void validate(String string) throws ValidationFailedException {
        if (!string.matches(REGEX)) {
            throw new ValidationFailedException("Validation is not pass");
        }
    }
}