package by.training.gorodetskaya;

public interface Validator<T> {
    void validate(T object) throws ValidationFailedException;
}