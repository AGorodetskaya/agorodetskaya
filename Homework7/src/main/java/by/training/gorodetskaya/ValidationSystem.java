package by.training.gorodetskaya;

public class ValidationSystem {
    private ValidatorFactory validatorFactory = new ValidatorFactory();

    public ValidatorFactory getValidatorFactory() {
        return validatorFactory;
    }

    public void validate(Object ob) throws ValidationFailedException {
        if (ob instanceof Integer) {
            getValidatorFactory().createIntegerValidator().validate((Integer) ob);
        } else if (ob instanceof String) {
            getValidatorFactory().createStringValidator().validate((String) ob);
        } else throw new ValidationFailedException("Unsupported type!");
    }
}