package by.training.gorodetskaya;

public class IntegerValidator implements Validator<Integer> {

    public void validate(Integer integer) throws ValidationFailedException {
        if (!((integer > 0) && (integer <= 10))) {
            throw new ValidationFailedException("Validation is not pass");
        }
    }
}