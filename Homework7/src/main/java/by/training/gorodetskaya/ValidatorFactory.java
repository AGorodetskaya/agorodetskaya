package by.training.gorodetskaya;

public class ValidatorFactory {

    public IntegerValidator createIntegerValidator() {
        return new IntegerValidator();
    }

    public StringValidator createStringValidator() {
        return new StringValidator();
    }
}
