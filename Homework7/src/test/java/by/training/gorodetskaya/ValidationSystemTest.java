package by.training.gorodetskaya;

import org.junit.Before;
import org.junit.Test;

public class ValidationSystemTest {
    private ValidationSystem validationSystem;

    @Before
    public void createValidationSystem() {
        validationSystem = new ValidationSystem();
    }

    @Test
    public void testValidateInt() throws ValidationFailedException {
        validationSystem.validate(1);
        validationSystem.validate(5);
        validationSystem.validate(10);
    }

    @Test(expected = ValidationFailedException.class)
    public void testValidateIntFails() throws ValidationFailedException {
        validationSystem.validate(11);
    }

    @Test(expected = ValidationFailedException.class)
    public void testValidateIntFails2() throws ValidationFailedException {
        validationSystem.validate(0);
    }

    @Test
    public void testValidateString() throws ValidationFailedException {
        validationSystem.validate("Hello");
        validationSystem.validate("Hello world, abc");
    }

    @Test(expected = ValidationFailedException.class)
    public void testValidateStringFails() throws ValidationFailedException {
        validationSystem.validate("hello");
    }

    @Test(expected = ValidationFailedException.class)
    public void testValidateStringFails2() throws ValidationFailedException {
        validationSystem.validate("");
    }
}