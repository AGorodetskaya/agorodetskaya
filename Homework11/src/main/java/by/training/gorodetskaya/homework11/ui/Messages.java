package by.training.gorodetskaya.homework11.ui;

public final class Messages {
    public final static String MAIN_MESSAGE = "DOWNLOAD - for getting last state of file system" + "\n" +
            "ENTER -  for creating object of file system" + "\n" + "PRINT -  for printing file system" + "\n" +
            "SAVE - for saving file system state" + "\n" + "EXIT - for finishing session";

    public final static String FIN_MESSAGE = "Session is finished";
    public final static String PATH_MESSAGE = "Enter the path formatted like root/folder/file.txt";
    public final static String INCORRECT_PATH = "Incorrect path!";
    public final static String INCORRECT_COMMAND = "Incorrect command!";
    public final static String WRITTEN_PATH = "Path was written";
    public final static String SAVING_MESSAGE = "File system is saved";
    public final static String DOWNLOADING_MESSAGE = "Last state of file system is downloaded";

    public static void printMainMessage() {
        System.out.println(MAIN_MESSAGE);
    }

    public static void printFinMessage() {
        System.out.println(FIN_MESSAGE);
    }

    public static void printPathMessage() {
        System.out.println(PATH_MESSAGE);
    }

    public static void printIncorrectPath() {
        System.out.println(INCORRECT_PATH);
    }

    public static void printWrittenPath() {
        System.out.println(WRITTEN_PATH);
    }

    public static void printIncorrectCommand() {
        System.out.println(INCORRECT_COMMAND);
    }

    public static void printSavingMessage() { System.out.println(SAVING_MESSAGE); }

    public static void printDownloadingMessage() { System.out.println(DOWNLOADING_MESSAGE); }
}