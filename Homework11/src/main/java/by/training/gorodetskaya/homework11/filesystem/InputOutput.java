package by.training.gorodetskaya.homework11.filesystem;

import by.training.gorodetskaya.homework11.model.Element;
import by.training.gorodetskaya.homework11.model.Folder;
import com.sun.corba.se.pept.transport.ReaderThread;

import java.io.*;

public class InputOutput {

    private static final String INTEND = "";
    private static final String ROOT_STORAGE = "StateOfSystem.bin";


    public void writeStateOfSystem(Element element) {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(ROOT_STORAGE))) {
            outputStream.writeObject(element);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Element readStateOfSystem() {
        Element element;
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(ROOT_STORAGE))) {
            element = (Element) inputStream.readObject();
            element.printElement(INTEND);
        } catch (IOException | ClassNotFoundException e) {
            return new Folder("root");
        }
        return element;
    }
}