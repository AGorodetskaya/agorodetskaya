package by.training.gorodetskaya.homework11.ui;

import by.training.gorodetskaya.homework11.filesystem.FileSystem;
import by.training.gorodetskaya.homework11.filesystem.InputOutput;
import by.training.gorodetskaya.homework11.filesystem.ElementHelper;

import java.util.Scanner;

public class UserInterface {
    private final FileSystem fileSystem = new FileSystem(new ElementHelper());
    private final InputOutput inputOutput = new InputOutput();

    public void workWithUser() {

        Messages.printMainMessage();

        Scanner scanner = new Scanner(System.in);
        String string = "";
        if (scanner.hasNext()) {
            string = scanner.next();
        }
        Command action = Command.lookUp(string);

        switch (action) {
            case DOWNLOAD:
                fileSystem.downloadRootFromLastState(inputOutput.readStateOfSystem());
                Messages.printDownloadingMessage();
                workWithUser();
                break;
            case ENTER:
                fileSystem.putFolderIntoStructure(fileSystem.getRoot(), fileSystem.makeInput());
                workWithUser();
                break;
            case PRINT:
                fileSystem.printFileSystem(fileSystem.getRoot());
                workWithUser();
                break;
            case SAVE:
                inputOutput.writeStateOfSystem(fileSystem.getRoot());
                Messages.printSavingMessage();
                workWithUser();
                break;
            case EXIT:
                Messages.printFinMessage();
                break;
            case DEFAULT:
                Messages.printIncorrectCommand();
                workWithUser();
                break;
        }
    }
}