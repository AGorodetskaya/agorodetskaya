package by.training.gorodetskaya.homework11.ui;

public enum Command {
    DOWNLOAD ("download"),
    ENTER("enter"),
    PRINT("print"),
    SAVE ("save"),
    EXIT("exit"),
    DEFAULT("");

    private final String userString;

    Command (String userString){
        this.userString = userString;
    }

    public String getUserString(){
        return userString;
    }

    public static Command lookUp(String userString){
        for (Command command : values()) {
            if (command.getUserString().toLowerCase().equals(userString.toLowerCase())) {
                return command;
            }
        }
    return DEFAULT;
    }
}