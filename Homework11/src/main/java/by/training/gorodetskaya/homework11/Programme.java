package by.training.gorodetskaya.homework11;

import by.training.gorodetskaya.homework11.ui.UserInterface;

public class Programme {
    public static void main(String[] args) {
        UserInterface start = new UserInterface();
        start.workWithUser();
    }
}