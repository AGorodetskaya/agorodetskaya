package by.training.gorodetskaya.homework11.filesystem;

import by.training.gorodetskaya.homework11.ui.Messages;
import by.training.gorodetskaya.homework11.model.Element;
import by.training.gorodetskaya.homework11.model.Folder;

import java.util.LinkedList;
import java.util.Scanner;

public class FileSystem {

    private static final String INTEND = "";
    private Element root = new Folder("root");
    private ElementHelper elementHelper;

    public FileSystem(ElementHelper elementHelper) {
        this.elementHelper = elementHelper;
    }

    public Element getRoot() {
        return root;
    }

    public void setRoot(Folder root) {
        this.root = root;
    }

    public LinkedList<Element> makeInput() {
        Messages.printPathMessage();
        Scanner scanner = new Scanner(System.in);
        String string = "";
        if (scanner.hasNext()) {
            string = scanner.next();
        }
        if (!elementHelper.isValidString(string)) {
            Messages.printIncorrectPath();
            return makeInput();
        }
        LinkedList<Element> list = elementHelper.createLinkedListFromString(string);
        Messages.printWrittenPath();
        return list;
    }

    public Element putFolderIntoStructure(Element root, LinkedList<Element> list) {
        while (!list.isEmpty()) {
            if (!root.containsElement(list.getFirst())) {
                root.putElement(elementHelper.setLinkedListToOuterFolder(list));
            } else {
                Element firstInRoot = root.getElement(list.poll());
                putFolderIntoStructure(firstInRoot, list);
            }
        }
        return root;
    }

    public void downloadRootFromLastState(Element element) {
        this.root = element;
    }

    public void printFileSystem(Element element) {
        element.printElement(INTEND);
    }
}