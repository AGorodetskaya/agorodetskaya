package by.training.gorodetskaya.homework5;

import by.training.gorodetskaya.homework5.model.HelpService;

import java.util.Scanner;

public class UserInterface {
    private final FileSystem fileSystem = new FileSystem(new HelpService());

    public void workWithUser() {

        Messages.printMainMessage();

        Scanner scanner = new Scanner(System.in);
        String string = "";
        if (scanner.hasNext()) {
            string = scanner.next();
        }
        Command action = Command.lookUp(string);
        switch (action) {
            case ENTER:
                fileSystem.putFolderIntoStructure(fileSystem.getRoot(), fileSystem.makeInput());
                workWithUser();
                break;
            case PRINT:
                fileSystem.printFileSystem(fileSystem.getRoot());
                workWithUser();
                break;
            case EXIT:
                Messages.printFinMessage();
                break;
            case DEFAULT:
                Messages.printIncorrectCommand();
                workWithUser();
                break;
        }
    }
}