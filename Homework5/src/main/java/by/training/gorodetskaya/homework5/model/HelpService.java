package by.training.gorodetskaya.homework5.model;

import java.util.LinkedList;

public class HelpService {
    private static final String SLASH = "/";
    private static final String DOT = ".";

    public LinkedList<Element> createLinkedListFromString(String string) {
        String[] array = string.split(SLASH);
        LinkedList<Element> list = new LinkedList<>();
        for (String name : array) {
            Element element;
            if (name.contains(DOT)) {
                element = new File(name);
            } else {
                element = new Folder(name);
            }
            list.add(element);
        }
        return list;
    }

    public boolean isValidString(String string) {
        if (!string.contains(SLASH)) {
            return false;
        }
        String[] array = string.split(SLASH);
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i].contains(DOT)) {
                return false;
            }
        }
        return true;
    }

    public Element setLinkedListToOuterFolder(LinkedList<Element> list) {
        Element outerElement = list.getFirst();
        while (list.size() > 1) {
            list.poll().putElement(list.getFirst());
        }
        list.poll();
        return outerElement;
    }
}