package by.training.gorodetskaya.homework5.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Folder implements Element {

    private static final String SLASH = "/";
    private static final String THREESPACES = "   ";
    private String name;
    private Map<String, Element> elements = new HashMap<>();

    public Folder(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Element> getElements() {
        return elements;
    }

    @Override
    public void printElement(String intend) {
        System.out.println(intend + name + SLASH);
        intend = intend + THREESPACES;
        for (Element folder : getElements().values()) {
            folder.printElement(intend);
        }
    }

    public void setElements(Map<String, Element> elements) {
        this.elements = elements;
    }

    @Override
    public void putElement(Element element) {
        elements.put(element.getName(), element);
    }

    @Override
    public boolean containsElement(Element element) {
        return elements.containsKey(element.getName());
    }

    @Override
    public Element getElement(Element element) {
        return elements.get(element.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Folder folder = (Folder) o;
        return name.equals(folder.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }
}