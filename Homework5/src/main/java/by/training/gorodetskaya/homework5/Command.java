package by.training.gorodetskaya.homework5;

public enum Command {
    ENTER("enter"), PRINT("print"), EXIT("exit"), DEFAULT("");

    private final String userString;

    Command (String userString){
        this.userString = userString;
    }

    public String getUserString(){
        return userString;
    }

    public static Command lookUp(String userString){
        for (Command command : values()) {
            if (command.getUserString().toLowerCase().equals(userString.toLowerCase())) {
                return command;
            }
        }
    return DEFAULT;
    }
}