package by.training.gorodetskaya.homework5.model;

import java.util.Map;
import java.util.Objects;

public class File implements Element {
    private static final String MESSAGE = "File can not contain anything";
    private String name;

    public File(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public Map<String, Element> getElements() {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public void printElement(String intend) {
        System.out.println(intend + name);
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void putElement(Element element) {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public boolean containsElement(Element element) {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public Element getElement(Element element) {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        File file = (File) o;
        return name.equals(file.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }
}