package by.training.gorodetskaya.homework5;

import by.training.gorodetskaya.homework5.model.Element;
import by.training.gorodetskaya.homework5.model.File;
import by.training.gorodetskaya.homework5.model.Folder;
import by.training.gorodetskaya.homework5.model.HelpService;

import java.util.LinkedList;
import java.util.Scanner;

public class FileSystem {
    private static final String INTEND = "";
    private Folder root = new Folder("root");
    private HelpService helpService;

    public FileSystem(HelpService helpService) {
        this.helpService = helpService;
    }

    public Folder getRoot() {
        return root;
    }

    public void setRoot(Folder root) {
        this.root = root;
    }

    public LinkedList<Element> makeInput() {
        Messages.printPathMessage();
        Scanner scanner = new Scanner(System.in);
        String string = "";
        if (scanner.hasNext()) {
            string = scanner.next();
        }
        if (!helpService.isValidString(string)) {
            Messages.printIncorrectPath();
            return makeInput();
        }
        LinkedList<Element> list = helpService.createLinkedListFromString(string);
        Messages.printWrittenPath();
        return list;
    }

    public Element putFolderIntoStructure(Element root, LinkedList<Element> list) {
        while (!list.isEmpty()) {
            if (!root.containsElement(list.getFirst())) {
                root.putElement(helpService.setLinkedListToOuterFolder(list));
            } else {
                Element firstInRoot = root.getElement(list.poll());
                putFolderIntoStructure(firstInRoot, list);
            }
        }
        return root;
    }

    public void printFileSystem(Element element) {
        element.printElement(INTEND);
    }
}