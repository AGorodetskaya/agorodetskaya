package by.training.gorodetskaya.homework5.model;

import java.util.Map;

public interface Element {
    void putElement(Element element);

    boolean containsElement(Element element);

    Element getElement(Element element);

    String getName();

    Map<String, Element> getElements();

    void printElement(String intend);
}