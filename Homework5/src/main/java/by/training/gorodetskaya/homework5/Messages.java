package by.training.gorodetskaya.homework5;

public final class Messages {
    public final static String MAIN_MESSAGE = "ENTER -  for creating object of file system" + "\n" +
            "PRINT -  for printing file system" + "\n" + "EXIT - for finishing session";
    public final static String FIN_MESSAGE = "Session is finished";
    public final static String PATH_MESSAGE = "Enter the path formatted like root/folder/file.txt";
    public final static String INCORRECT_PATH = "Incorrect path!";
    public final static String INCORRECT_COMMAND = "Incorrect command!";
    public final static String WRITTEN_PATH = "Path was written";

    public static void printMainMessage() {
        System.out.println(MAIN_MESSAGE);
    }

    public static void printFinMessage() {
        System.out.println(FIN_MESSAGE);
    }

    public static void printPathMessage() {
        System.out.println(PATH_MESSAGE);
    }

    public static void printIncorrectPath() {
        System.out.println(INCORRECT_PATH);
    }

    public static void printWrittenPath() {
        System.out.println(WRITTEN_PATH);
    }

    public static void printIncorrectCommand() {
        System.out.println(INCORRECT_COMMAND);
    }
}