package by.training.gorodetskaya.homework5.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FolderTest {
    private Folder folder;
    private Folder innerFolder;
    private File file;

    @Before
    public void initiate()  {
        folder = new Folder("Folder");
        innerFolder = new Folder("Inner folder");
        file = new File("File");
    }

    @Test
    public void testGetElements() {
        folder.getElements().put("Inner folder", innerFolder);
        innerFolder.getElements().put("File", file);
        Assert.assertEquals(1, folder.getElements().size());
        Assert.assertEquals(1, innerFolder.getElements().size());

    }

    @Test
    public void testPutElement() {
        folder.getElements().put("Inner folder", innerFolder);
        Assert.assertEquals(1, folder.getElements().size());
    }

    @Test
    public void testContainsElement() {
        folder.getElements().put("File", file);
        assertTrue(folder.containsElement(file));
    }
    @Test
    public void testNotContainsElement(){
        innerFolder.getElements().put("File", file);
        folder.getElements().put("Inner folder", innerFolder);
        assertFalse(folder.containsElement(file));
    }
}