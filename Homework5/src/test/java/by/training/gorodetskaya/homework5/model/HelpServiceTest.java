package by.training.gorodetskaya.homework5.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

public class HelpServiceTest {
    HelpService helpService;
    String string;
    String fileString;
    String nullString;
    String wrongString;
    String anotherWrongString;
    String oneMoreWrongString;
    LinkedList<Element> list;
    Folder animal;
    Folder cat;
    Folder boba;
    File vasya;

    @Before
    public void initiate() {
        helpService = new HelpService();
        string = "animal/cat/boba";
        list = new LinkedList<>();
        animal = new Folder("animal");
        cat = new Folder("cat");
        boba = new Folder("boba");
        fileString = "animal/cat/vasya.txt";
        vasya = new File("vasya.txt");
        nullString = "///";
        wrongString = "animal/cat.txt/biba";
        anotherWrongString = "animal dog tuzik";
        oneMoreWrongString = "animaldogtuzik";
    }

    @Test
    public void testCreateLinkedListFromStringCorrect() {
        list.add(animal);
        list.add(cat);
        list.add(boba);
        Assert.assertEquals(list, helpService.createLinkedListFromString(string));
    }

    @Test
    public void testCreateLinkedListFromStringCorrectWithFiles() {
        list.add(animal);
        list.add(cat);
        list.add(vasya);
        Assert.assertEquals(list, helpService.createLinkedListFromString(fileString));
    }

    @Test
    public void testCreateLinkedListNull() {
        Assert.assertEquals(list, helpService.createLinkedListFromString(nullString));
    }

    @Test
    public void testIsValidStringTrue() {
        Assert.assertTrue(helpService.isValidString(string));
        Assert.assertTrue(helpService.isValidString(fileString));
        Assert.assertTrue(helpService.isValidString(nullString));
    }

    @Test
    public void testIsValidStringFalse() {
        Assert.assertFalse(helpService.isValidString(wrongString));
        Assert.assertFalse(helpService.isValidString(anotherWrongString));
        Assert.assertFalse(helpService.isValidString(oneMoreWrongString));
    }

    @Test
    public void testSetLinkedlistToOuterFolder() {
        list.add(animal);
        list.add(cat);
        list.add(boba);
        Assert.assertEquals(animal, helpService.setLinkedListToOuterFolder(list));
    }
}