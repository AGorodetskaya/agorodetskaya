package by.training.gorodetskaya.homework5.model;

import org.junit.Before;
import org.junit.Test;


public class FileTest {
    private File file;
    private Folder folder;

    @Before
    public void initiate (){
        file = new File("File");
        folder = new Folder("Folder");
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testGetElements()throws UnsupportedOperationException {
        file.getElements();
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testGetElement()throws UnsupportedOperationException {
        file.getElement(folder);
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testPutElement()throws UnsupportedOperationException {
        file.putElement(folder);
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testContainsElement()throws UnsupportedOperationException {
        file.containsElement(folder);
    }
}