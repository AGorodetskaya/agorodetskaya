package by.training.gorodetskaya.homework6;

import org.junit.Assert;
import org.junit.Test;

public class StringAnalyzerTest {
    StringAnalyzer stringAnalyzer = new StringAnalyzer();
    String string = "Going through the forest is my favourite part of the walk. My dog Benji loves " +
            "it too. I’m Grace. I live on a farm with my parents and I take Benji for a walk most" +
            " days after school. While Benji’s playing, I stop to take a photo of a butterfly.";
    String wrongString = " ";

    @Test
    public void getWordsFromStringIsCorrect() {
        /*Given*/
        String expected = "[Going, through, the, forest, is, my, favourite, part, of, the, walk, My, dog, Benji, " +
                "loves, it, too, I’m, Grace, I, live, on, a, farm, with, my, parents, and, I, take, Benji, for, a, " +
                "walk, most, days, after, school, While, Benji’s, playing, I, stop, to, take, a, photo, of, a, butterfly]";
        /*When*/
        String actual = stringAnalyzer.getWordsFromString(string).toString();
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getSortedUniqueWordsFromListIsCorrect() {
        /*Given*/
        String expected = "[Benji, Benji’s, Going, Grace, I, I’m, My, a, after, and, butterfly, days, dog, farm, " +
                "favourite, for, forest, is, it, live, loves, most, my, of, on, parents, part, photo, playing, " +
                "school, While, stop, take, the, through, to, too, walk, with]";
        /*When*/
        String actual = stringAnalyzer.getSortedUniqueWordsFromList(stringAnalyzer.getWordsFromString(string)).toString();
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void chooseFirstLetterIsCorrect() {
        /*Given*/
        String expected = "[a, b, d, f, g, i, l, m, o, p, s, t, w]";
        /*When*/
        String actual = stringAnalyzer.chooseFirstLetter(stringAnalyzer.getSortedUniqueWordsFromList(stringAnalyzer.
                getWordsFromString(string))).toString();
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void makeMapWordIsCorrect() {
        /*Given*/
        String expected = "{a=[a, after, and], b=[Benji, Benji’s, butterfly], d=[days, dog], f=[farm, favourite, for, " +
                "forest], g=[Going, Grace], i=[I, I’m, is, it], l=[live, loves], m=[My, most, my], o=[of, on], " +
                "p=[parents, part, photo, playing], s=[school, stop], t=[take, the, through, to, too], " +
                "w=[While, walk, with]}";
        /*When*/
        String actual = stringAnalyzer.makeMapWord(stringAnalyzer.chooseFirstLetter(stringAnalyzer.
                getSortedUniqueWordsFromList(stringAnalyzer.getWordsFromString(string))), stringAnalyzer.
                getSortedUniqueWordsFromList(stringAnalyzer.getWordsFromString(string))).toString();
        /*Then*/
        Assert.assertEquals(expected, actual);
    }
}