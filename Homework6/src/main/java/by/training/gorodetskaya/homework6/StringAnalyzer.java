package by.training.gorodetskaya.homework6;

import java.util.*;

public class StringAnalyzer {

    private static final String SPACE = " ";
    private static final String DOT = ".";
    private static final String COMMA = ",";
    private static final String EMPTY_STRING = "";
    private static final String INTEND = "   ";

    public List<String> getWordsFromString(String string) {
        string = string.replace(COMMA, EMPTY_STRING);
        string = string.replace(DOT, EMPTY_STRING);
        String[] words = string.split(SPACE);
        return Arrays.asList(words);
    }

    public List<String> getSortedUniqueWordsFromList(List<String> wordList) {
        Set<String> set = new HashSet<>(wordList);
        List<String> distinctList = new ArrayList<>(set);
        Collections.sort(distinctList);
        return distinctList;
    }

    public List<String> chooseFirstLetter(List<String> list) {
        Set<String> lettersSet = new HashSet<>();
        for (String word : list) {
            lettersSet.add(String.valueOf(word.toLowerCase().charAt(0)));
        }
        List<String> lettersList = new ArrayList<>(lettersSet);
        Collections.sort(lettersList);
        return lettersList;
    }

    public Map<String, List<String>> makeMapWord(List<String> lettersList, List<String> distinctList) {
        Map<String, List<String>> mapWord = new TreeMap<>();
        for (String letter : lettersList) {
            List<String> wordsPerLetter = new ArrayList<>();
            for (String word : distinctList) {
                if (word.toLowerCase().startsWith(letter.toLowerCase())) {
                    wordsPerLetter.add(word);
                }
                mapWord.put(letter, wordsPerLetter);
            }
        }
        return mapWord;
    }

    public void printMapWord(Map<String, List<String>> mapWord, List<String> wordList) {
        for (String letter : mapWord.keySet()) {
            List<String> value = mapWord.get(letter);
            System.out.println(letter + ": " + value.get(0) + SPACE + Collections.frequency(wordList, value.get(0)));
            for (int i = 1; i < value.size(); i++) {
                String uniqueWord = value.get(i);
                System.out.println(INTEND + uniqueWord + SPACE + Collections.frequency(wordList, uniqueWord));
            }
        }
    }
}