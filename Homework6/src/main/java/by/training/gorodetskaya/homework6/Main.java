package by.training.gorodetskaya.homework6;

import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        String string = "Going through the forest is my favourite part of the walk. My dog Benji loves " +
                "it too. I’m Grace. I live on a farm with my parents and I take Benji for a walk most" +
                " days after school. While Benji’s playing, I stop to take a photo of a butterfly.";

        StringAnalyzer stringAnalyzer = new StringAnalyzer();
        List<String> words = stringAnalyzer.getWordsFromString(string);
        List<String> strings = stringAnalyzer.getSortedUniqueWordsFromList(stringAnalyzer.getWordsFromString(string));
        List<String> lettersList = stringAnalyzer.chooseFirstLetter(strings);
        Map<String, List<String>> mapWord = stringAnalyzer.makeMapWord(lettersList, strings);
        stringAnalyzer.printMapWord(mapWord, words);
    }
}
