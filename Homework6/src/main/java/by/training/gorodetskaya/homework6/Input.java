package by.training.gorodetskaya.homework6;

import java.util.Scanner;

public class Input {

    public String getStringFromConsole() {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        return string;
    }
}