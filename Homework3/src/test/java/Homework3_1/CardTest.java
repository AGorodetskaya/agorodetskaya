package Homework3_1;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {
    Card card = new Card("Иванов Иван Иванович", 150);
    public static final double DELTA = 0.1d;

    @Test
    public void testGetFromIsCorrect() {
        /*Given*/
        int expected = 150;
        /*When*/
        int actual = card.getFrom();
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testAddToIsCorrect() {
        /*Given*/
        int expected = 240;
        /*When*/
        int actual = card.addTo(90);
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testWithDrawIsCorrect() {
        /*Given*/
        int expected = 100;
        /*When*/
        int actual = card.withDraw(50);
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testConverseIsCorrect() {
        /*Given*/
        double expected = 58.59d;
        /*When*/
        double actual = card.converse(1);
        /*Then*/
        Assert.assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetFromIsIncorrect() {
        /*Given*/
        int expected = -100;
        /*When*/
        int actual = card.getFrom();
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void testAddToIsIncorrect() {
        /*Given*/
        int expected = 60;
        /*When*/
        int actual = card.addTo(-90);
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void testWithDrawIsIncorrect() {
        /*Given*/
        int expected = -50;
        /*When*/
        int actual = card.withDraw(200);
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void testConverseIsIncorrect() {
        /*Given*/
        double expected = 58.59d;
        /*When*/
        double actual = card.converse(0);
        /*Then*/
        Assert.assertNotEquals(expected, actual, DELTA);
    }
}

