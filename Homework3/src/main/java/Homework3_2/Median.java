package Homework3_2;

import java.util.Arrays;

public class Median {
    public static float median(int[] arr) {
        float result = 0.0f;
        Arrays.sort(arr);
        if (arr.length % 2 == 0) {
            int i = arr.length / 2;
            result = (float) (arr[i] + arr[i - 1]) / 2;
        } else if (arr.length % 2 == 1) {
            int i = arr.length / 2;
            result = (float) arr[i];
        }
        return result;
    }

    public static double median(double[] arr) {
        double result = 0.0d;
        Arrays.sort(arr);
        if (arr.length % 2 == 0) {
            int i = arr.length / 2;
            result = (arr[i] + arr[i - 1]) / 2;
        } else if (arr.length % 2 == 1) {
            int i = arr.length / 2 + 1;
            result = arr[i - 1];
        }
        return result;
    }
}
