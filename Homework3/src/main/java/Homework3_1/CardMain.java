package Homework3_1;

import java.util.Scanner;

public class CardMain {

    public static void main(String[] args) {
        System.out.print("Print 1 for getting balance, 2 for adding money to account, 3 for taking money " +
                "over account, 4 for conversion and press Enter");
        Scanner scanner = new Scanner(System.in);
        int i = 0;
        if (scanner.hasNextInt()) i = scanner.nextInt();

        Card card = new Card("Иванов Иван Иванович", 150);

        if (i == 1) {
            card.getFrom();
        }
        if (i == 2) {
            System.out.print("Print amount");
            Scanner scanner1 = new Scanner(System.in);
            int sum_1 = 0;
            if (scanner1.hasNextInt()) sum_1 = scanner1.nextInt();
            card.addTo(sum_1);
        }
        if (i == 3) {
            System.out.print("Print amount");
            Scanner scanner2 = new Scanner(System.in);
            int sum_2 = 0;
            if (scanner2.hasNextInt()) sum_2 = scanner2.nextInt();
            card.withDraw(sum_2);
        }
        if (i == 4) {
            System.out.print("Print currency - 1 for USD, 2 for EUR, 3 for RUB");
            Scanner scanner3 = new Scanner(System.in);
            int cur = 0;
            if (scanner3.hasNextInt()) cur = scanner3.nextInt();
            card.converse(cur);
        }
    }
}


