package Homework3_1;

public class Card {
    int balance;
    private String name;

    Card(String name, int balance) {
        this.balance = balance;
        this.name = name;
    }

    Card(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFrom() {
        if (balance >= 0) {
            System.out.print("Your balance - " + balance + " BYN");
        } else {
            System.out.print("Error, balance can not be negative");
        }
        return balance;
    }

    public int addTo(int sum_1) {
        if (sum_1 >= 0) {
            balance = balance + sum_1;
            System.out.print("Your balance - " + balance + " BYN");
        }
        return balance;
    }

    public int withDraw(int sum_2) {
        if (sum_2 >= 0 && balance >= sum_2) {
            balance = balance - sum_2;
            System.out.print("Your balance - " + balance + " BYN");
        } else {
            System.out.print("Not enough money! Your balance - " + balance + " BYN");
        }
        return balance;
    }

    public double converse(int cur) {
        double usd = 2.56;
        double eur = 3.03;
        double rub = 0.033;
        double balanceCur = 0.0;
        if (cur == 1) {
            balanceCur = (double) (balance / usd);
            System.out.printf("Your balance in USD - " + "%.2f", balanceCur);
        } else if (cur == 2) {
            balanceCur = (double) (balance / eur);
            System.out.printf("Your balance in EUR - " + "%.2f", balanceCur);
        } else if (cur == 3) {
            balanceCur = (double) (balance / rub);
            System.out.printf("Your balance in RUB - " + "%.2f", balanceCur);
        } else {
            System.out.print("Please, choose 1, 2 or 3 for necessary currency");
        }
        return balanceCur;
    }
}




