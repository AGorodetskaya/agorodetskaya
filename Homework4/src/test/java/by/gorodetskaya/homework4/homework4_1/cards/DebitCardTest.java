package by.gorodetskaya.homework4.homework4_1.cards;

import by.gorodetskaya.homework4.homework4_1.cards.DebitCard;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DebitCardTest {
    private DebitCard debitCard;
    public static final double DELTA = 0.1d;

    @Before
    public void initiateCard() {
        debitCard = new DebitCard("Василий Васильевич Васильев", 500);
    }

    @Test
    public void testShowBalanceIsCorrect() {
        /*Given*/
        int expected = 500;
        /*When*/
        int actual = debitCard.showBalance();
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testAddToIsCorrect() {
        /*Given*/
        int expected = 530;
        /*When*/
        int actual = debitCard.addTo(30);
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testWithDrawIsCorrect() {
        /*Given*/
        int expected = 200;
        /*When*/
        int actual = debitCard.withDraw(300);
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testConverseIsCorrect() {
        /*Given*/
        double expected = 196.85d;
        /*When*/
        double actual = debitCard.converse("USD", 2.54);
        /*Then*/
        Assert.assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testShowBalanceIsIncorrect() {
        /*Given*/
        int expected = -100;
        /*When*/
        int actual = debitCard.showBalance();
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void testAddToIsIncorrect() {
        /*Given*/
        int expected = 400;
        /*When*/
        int actual = debitCard.addTo(-100);
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void testWithDrawIsIncorrect() {
        /*Given*/
        int expected = -200;
        /*When*/
        int actual = debitCard.withDraw(-700);
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void testConverseIsIncorrect() {
        /*Given*/
        double expected = 165.02d;
        /*When*/
        double actual = debitCard.converse("USD", 0.0);
        /*Then*/
        Assert.assertNotEquals(expected, actual, DELTA);

    }
}