package by.gorodetskaya.homework4.homework4_1;

import by.gorodetskaya.homework4.homework4_1.cards.Card;
import by.gorodetskaya.homework4.homework4_1.cards.CreditCard;
import by.gorodetskaya.homework4.homework4_1.cards.DebitCard;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AtmTest {
    private Atm atm;
    private Card card;
    private DebitCard debitCard;
    private CreditCard creditCard;

    @Before
    public void initiate() {
        atm = new Atm(card);
        card = new Card("Иванов Иван Иванович", 300);
        debitCard = new DebitCard("Иванов Иван Иванович", 250);
        creditCard = new CreditCard("Иванов Иван Иванович", 150);
    }

    @Test
    public void addToIsCorrect() {
        /*Given*/
        int expected = 450;
        /*When*/
        int actual = atm.addTo(card, 150);
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void withDrawIsCorrectDebit() {
        /*Given*/
        int expected = 0;
        /*When*/
        int actual = atm.withDraw(debitCard, 250);
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void withDrawIsCorrectCredit() {
        /*Given*/
        int expected = -50;
        /*When*/
        int actual = atm.withDraw(creditCard, 200);
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void addToIsIncorrect() {
        /*Given*/
        int expected = 100;
        /*When*/
        int actual = atm.addTo(card, -200);
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void withDrawIsIncorrectDebit() {
        /*Given*/
        int expected = -350;
        /*When*/
        int actual = atm.withDraw(debitCard, 600);
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void withDrawIsIncorrectCredit() {
        /*Given*/
        int expected = -50;
        /*When*/
        int actual = atm.withDraw(debitCard, 300);
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }
}