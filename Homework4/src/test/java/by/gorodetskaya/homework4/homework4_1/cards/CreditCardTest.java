package by.gorodetskaya.homework4.homework4_1.cards;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CreditCardTest {
    private CreditCard creditCard;
    public static final double DELTA = 0.1d;

    @Before
    public void initiateCard() {
        creditCard = new CreditCard("Петров Петр Петрович", 280);
    }

    @Test
    public void testShowBalanceIsCorrect() {
        /*Given*/
        int expected = 280;
        /*When*/
        int actual = creditCard.showBalance();
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testAddToIsCorrect() {
        /*Given*/
        int expected = 380;
        /*When*/
        int actual = creditCard.addTo(100);
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testWithDrawIsCorrect() {
        /*Given*/
        int expected = -20;
        /*When*/
        int actual = creditCard.withDraw(300);
        /*Then*/
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testConverseIsCorrect() {
        /*Given*/
        double expected = 110.23d;
        /*When*/
        double actual = creditCard.converse("USD", 2.54);
        /*Then*/
        Assert.assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testShowBalanceIsIncorrect() {
        /*Given*/
        int expected = -100;
        /*When*/
        int actual = creditCard.showBalance();
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void testAddToIsIncorrect() {
        /*Given*/
        int expected = 60;
        /*When*/
        int actual = creditCard.addTo(-90);
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void testWithDrawIsIncorrect() {
        /*Given*/
        int expected = 80;
        /*When*/
        int actual = creditCard.withDraw(-200);
        /*Then*/
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void testConverseIsIncorrect() {
        /*Given*/
        double expected = 0.0d;
        /*When*/
        double actual = creditCard.converse("USD", 0.0);
        /*Then*/
        Assert.assertNotEquals(expected, actual, DELTA);

    }
}