package by.gorodetskaya.homework4.homework4_2;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SelectionSortTest {
    int[] array = {1, 5, 10, 8, 4};

    @Test
    public void testSortIsCorrect() {
        /*Given*/
        int[] expected = {1, 4, 5, 8, 10};
        Sorter sortStrategy = new SelectionSort();
        /*When*/
        int[] actual = sortStrategy.sort(array);
        /*Then*/
        Assert.assertArrayEquals(expected, actual);
    }
}