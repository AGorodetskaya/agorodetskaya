package by.gorodetskaya.homework4.homework4_1.cards;

import by.gorodetskaya.homework4.homework4_1.cards.Card;

public class DebitCard extends Card {
    public DebitCard(String name, int balance) {
        super(name, balance);
    }
}