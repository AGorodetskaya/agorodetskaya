package by.gorodetskaya.homework4.homework4_1;

import by.gorodetskaya.homework4.homework4_1.cards.Card;
import by.gorodetskaya.homework4.homework4_1.cards.CreditCard;
import by.gorodetskaya.homework4.homework4_1.cards.DebitCard;

public class Atm {

    private Card card;

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Atm(Card card) {
        this.card = card;
    }

    public int addTo(Card card, int amount) {
        return card.addTo(amount);
    }

    public int withDraw(DebitCard debitCard, int sum) {
        return debitCard.withDraw(sum);
    }

    public int withDraw(CreditCard creditCard, int sum) {
        return creditCard.withDraw(sum);
    }
}