package by.gorodetskaya.homework4.homework4_1.cards;


public class Card {
    private int balance;
    private String name;

    public Card(String name, int balance) {
        this.balance = balance;
        this.name = name;
    }

    public Card(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int showBalance() {
        if (getBalance() >= 0) {
            System.out.printf("Your balance - %s BYN", balance);
        }
        return balance;
    }

    public int addTo(int amount) {
        if (amount >= 0) {
            balance = getBalance() + amount;
            System.out.printf("Your balance - %s BYN", balance);
        }
        return balance;
    }

    public int withDraw(int sum) {
        if (sum >= 0 && getBalance() >= sum) {
            balance = getBalance() - sum;
            System.out.printf("Your balance - %s BYN", balance);
        } else {
            System.out.printf("Not enough money! Your balance - %s BYN", balance);
        }
        return balance;
    }

    public double converse(String currency, double course) {
        double balanceCurrency;
        if (course > 0.0) {
            balanceCurrency = balance / course;
            System.out.printf(String.format("Your balance in %s %%.2f", currency), balanceCurrency);
        } else {
            balanceCurrency = balance;
        }
        return balanceCurrency;
    }
}