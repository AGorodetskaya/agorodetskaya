package by.gorodetskaya.homework4.homework4_1.cards;

public class CreditCard extends Card {
    public CreditCard(String name, int balance) {
        super(name, balance);
    }

    public int withDraw(int sum) {
        int balance = getBalance() - sum;
        System.out.print("Your balance - " + balance + " BYN");
        return balance;
    }
}