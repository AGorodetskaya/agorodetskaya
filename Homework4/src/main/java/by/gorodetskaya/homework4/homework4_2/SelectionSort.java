package by.gorodetskaya.homework4.homework4_2;

import java.util.Arrays;

public class SelectionSort implements Sorter {
    public int[] sort(int[] array) {
        System.out.print("Сортировка выбором");
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minId = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minId = j;
                    int temp = array[i];
                    array[i] = min;
                    array[minId] = temp;
                }
            }
            System.out.print(Arrays.toString(array));
        }
        return (array);
    }
}