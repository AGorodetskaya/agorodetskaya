package by.gorodetskaya.homework4.homework4_2;

import java.util.Arrays;

public class SortingContext {
  Sorter sortStrategy;

  SortingContext(Sorter sortStrategy) {
    this.sortStrategy = sortStrategy;
  }

  public String execute(int[] array) {
    sortStrategy.sort(array);
    return Arrays.toString(array);
  }
}
