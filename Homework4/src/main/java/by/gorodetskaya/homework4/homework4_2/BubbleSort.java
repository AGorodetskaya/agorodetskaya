package by.gorodetskaya.homework4.homework4_2;

import java.util.Arrays;

public class BubbleSort implements Sorter {
    public int[] sort(int[] array) {
        System.out.print("Cортировка пузырьком");
        boolean isSorted = false;
        int temp;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    isSorted = false;
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
            System.out.print(Arrays.toString(array));
        }
        return (array);
    }
}