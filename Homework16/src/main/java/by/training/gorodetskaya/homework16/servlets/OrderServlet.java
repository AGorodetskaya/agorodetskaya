package by.training.gorodetskaya.homework16.servlets;

import by.training.gorodetskaya.homework16.helpers.OrderListBuildHelper;
import by.training.gorodetskaya.homework16.models.Product;
import by.training.gorodetskaya.homework16.services.BasketService;
import by.training.gorodetskaya.homework16.services.PriceListService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

@WebServlet(asyncSupported = false, name = "Order page", urlPatterns = "/order")
public class OrderServlet extends HttpServlet {

    private static final String NAME = "name";
    private static final String ID = "id";
    private static final String ORDER_PAGE = "/order";
    private static final String EMPTY_BASKET = "Make your order";
    private static final String NOT_EMPTY_BASKET = "You have already chosen:";
    private final PriceListService priceList = PriceListService.getInstance();
    private final BasketService basketService = BasketService.getInstance();
    private final OrderListBuildHelper orderListBuildHelper = OrderListBuildHelper.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        PrintWriter writer = resp.getWriter();
        writer.println("<html>" +
                "<body>" +
                "<br><br>" +
                "<h1 align=center>Hello, " + session.getAttribute(NAME) + "!" +
                "</h1>" +
                "<br><br>" +
                "<p align=center>" + makeHeaderOfPage((String) session.getAttribute(NAME)) + "</p>" +
                orderListBuildHelper.makeUserBasket(basketService.getUserOrder((String) session.getAttribute(NAME))) +
                "<form align=center action=\"" + req.getContextPath() + "/order\" method=\"post\" name=\"priceForm\">" +
                "<select name=\"id\" required=\"required\">" +
                makeSuggestionList() +
                "</select>" +
                "<br><br>" +
                "<input type= \"submit\" value=\"Add item\">" +
                "</form>" +
                "<br>" +
                "<form align=center action=\"" + req.getContextPath() + "/confirm\" method=\"get\"name=\"bucketUserForm\">" +
                "<input type=\"submit\" value=\"Submit\">" +
                "</form>" +
                "</body>" +
                "</html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        String id = request.getParameter(ID);
        Product product = priceList.getRange().get(Integer.parseInt(id));
        HttpSession session = request.getSession();
        basketService.addProduct((String) session.getAttribute(NAME), product);
        resp.sendRedirect(request.getContextPath() + ORDER_PAGE);
    }

    private String makeSuggestionList() {
        return priceList.getRange().entrySet().stream()
                .map(entry -> "<option name=\"id\" value=" + entry.getKey() + ">" + entry.getValue().toString() + "</option>")
                .collect(Collectors.joining());
    }

    private String makeHeaderOfPage(String userName) {
        return basketService.getAllUsersBasket().get(userName).isEmpty()
                ? EMPTY_BASKET
                : NOT_EMPTY_BASKET;
    }
}