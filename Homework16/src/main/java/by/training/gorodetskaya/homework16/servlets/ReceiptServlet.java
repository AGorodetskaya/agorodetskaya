package by.training.gorodetskaya.homework16.servlets;

import by.training.gorodetskaya.homework16.helpers.OrderListBuildHelper;
import by.training.gorodetskaya.homework16.services.BasketService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(asyncSupported = false, name = "Confirm page", urlPatterns = "/confirm")
public class ReceiptServlet extends HttpServlet {

    private static final String NAME = "name";
    private final BasketService basketService = BasketService.getInstance();
    private final OrderListBuildHelper orderListBuildHelper = OrderListBuildHelper.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        PrintWriter writer = resp.getWriter();
        writer.println("<html>" +
                "<body>" +
                "<br><br>" +
                "<h1 align=center>Dear " + session.getAttribute(NAME) + ", your order" +
                "</h1>" +
                "<br><br>" +
                orderListBuildHelper.makeUserBasket(basketService.getUserOrder((String) session.getAttribute(NAME))) +
                "<br>" +
                "<div align=center>Total: $ " + basketService.countAmount((String) session.getAttribute(NAME)) + "</div>" +
                "</body>" +
                "</html>");
    }
}