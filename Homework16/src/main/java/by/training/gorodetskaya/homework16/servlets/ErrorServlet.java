package by.training.gorodetskaya.homework16.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(asyncSupported = false, name = "Error page", urlPatterns = "/error")
public class ErrorServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter out = resp.getWriter();
        out.println("<html>" +
                "<body>" +
                "<br><br>" +
                "<h1 align=center>Oops!</h1>" +
                "<br><br>" +
                "<h3 align=center>You shouldn't be here</h3>" +
                "<h3 align=center>Please, agree with the terms of service first</h3>" +
                "<br><br>" +
                "<div align=center>" +
                "<a text-align:center href=\"" + req.getContextPath() + "/welcome\">Start page</a>" +
                "</div>" +
                "</body>" +
                "</html>");
    }
}