package by.training.gorodetskaya.homework16.helpers;

import by.training.gorodetskaya.homework16.models.Product;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OrderListBuildHelper {

    private static OrderListBuildHelper instance;

    private OrderListBuildHelper() {
    }

    public static synchronized OrderListBuildHelper getInstance() {
        if (instance == null) {
            instance = new OrderListBuildHelper();
        }
        return instance;
    }

    public String makeUserBasket(Collection<Product> collection) {

        String basketWithPlaceholders = collection.stream()
                .map(product -> "<div align=center>%s) " + product + "</div><br>")
                .collect(Collectors.joining());

        Object[] objects = Stream.iterate(1, n -> n + 1)
                .limit(collection.size())
                .toArray();

        return String.format(basketWithPlaceholders, objects);
    }
}
