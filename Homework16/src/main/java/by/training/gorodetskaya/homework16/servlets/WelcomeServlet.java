package by.training.gorodetskaya.homework16.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(asyncSupported = false, name = "Welcome page", urlPatterns = "/welcome")
public class WelcomeServlet extends HttpServlet {

    private static final String NAME = "name";
    private static final String AGREEMENT = "agreement";
    private static final String ORDER_PAGE = "/order";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter out = resp.getWriter();
        out.println("<html>" +
                "<body>" +
                "<br><br>" +
                "<h1 align=center>Welcome to Online Shop</h1>" +
                "<br><br>" +
                "<div align=center>" +
                "<form action=\"" + req.getContextPath() + "/welcome\" method=\"post\">" +
                "<input type=\"text\" id=\"name\" name=\"name\" placeholder=\"Enter your name\" required=\"required\">" +
                "<br><br><br>" +
                "<input type=\"checkbox\" id=\"agreement\" name=\"agreement\">" +
                "<label for=\"agreement\">I agree with the terms of service</label>" +
                "<br><br>" +
                "<input type=\"submit\" value=\"Enter\">" +
                "</form>" +
                "</div>" +
                "</body>" +
                "</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String name = req.getParameter(NAME);
        String agreement = req.getParameter(AGREEMENT);
        session.setAttribute(NAME, name);
        session.setAttribute(AGREEMENT, agreement);
        resp.sendRedirect(req.getContextPath() + ORDER_PAGE);
    }
}